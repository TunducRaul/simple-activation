package conference;

import conference.model.ConferenceEdition;
import conference.model.Reviewer;
import conference.model.SteeringCommitteeMember;
import conference.service.LoginService;
import conference.service.ServiceMock;
import conference.ui.iss_forms.LoginController;
import conference.utils.SpringAutowireConfig;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.logging.Logger;

public class StartMain extends Application {
    private static SessionFactory sessionFactory;
    private static ServiceMock serviceMock;

    public static void main(String[] args) {
        launch(args);
    }

    private static void close() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }

    private static void addEntities() {
        Transaction transaction = null;
        try {
            Session session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            SteeringCommitteeMember steeringCommitteeMember = new SteeringCommitteeMember(
                    "Travis", "Enty",
                    1, false, "travisenty", "enty"
            );
            ConferenceEdition conferenceEdition = new ConferenceEdition();
            // session.save(conferenceEdition);
//            conferenceEdition.addReviewer(reviewer);
            session.save(steeringCommitteeMember);

            transaction.commit();


        } catch (RuntimeException ignored) {
            if (transaction != null) {
                transaction.rollback();
            }
            ignored.printStackTrace();
            Logger.getLogger("Main").warning("Cannot save entity");
        }
    }

    private static void addMock() {
        Transaction transaction = null;
        Transaction transaction1 = null;
        try {
            Session session = sessionFactory.openSession();

            transaction = session.beginTransaction();
            SteeringCommitteeMember steeringCommitteeMember = new SteeringCommitteeMember(
                    "fname", "lname",
                    1, false, "uname", "pass"
            );
            SteeringCommitteeMember steeringCommitteeMember2 = new SteeringCommitteeMember(
                    "name", "lastName",
                    1, true, "admin", "admin"
            );
            ConferenceEdition conferenceEdition = new ConferenceEdition();
            // session.save(conferenceEdition);
//            conferenceEdition.addReviewer(reviewer);
            session.save(steeringCommitteeMember);
            session.save(steeringCommitteeMember2);
            transaction.commit();
            Session session1 = sessionFactory.openSession();
            transaction1 = session1.beginTransaction();
            SteeringCommitteeMember reviewer = new SteeringCommitteeMember("firstName", "lastName",
                    1, false, "username", "password");
            session1.save(reviewer);
            transaction1.commit();
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            List<Reviewer> reviewers = session1
                    .createQuery("from Reviewer ")
                    .getResultList();
            reviewer.setFirstName("SECOND_FIRSTNAME");
            session.update(reviewer);
            transaction.commit();
            reviewers.forEach((it) -> System.out.println(it.getFirstName()));
        } catch (RuntimeException ignored) {
            if (transaction1 != null) {
                transaction1.rollback();
            }
            ignored.printStackTrace();
            Logger.getLogger("Main").warning("Cannot save entity");
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringAutowireConfig.class);
        sessionFactory = context.getBean(SessionFactory.class);
        serviceMock = context.getBean(ServiceMock.class);
//        serviceMock.addMockData();
        LoginService service = context.getBean(LoginService.class);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/iss_forms/Login.fxml"));
        Parent root = loader.load();
        LoginController controller = loader.getController();
        controller.setLoginService(service);
        controller.setLoginStage(primaryStage);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            close();
            controller.closeAllStages();
        });
    }
}