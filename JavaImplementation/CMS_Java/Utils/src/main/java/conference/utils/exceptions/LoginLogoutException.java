package conference.utils.exceptions;

public class LoginLogoutException extends Exception{
    public LoginLogoutException() {
    }

    public LoginLogoutException(String message) {
        super(message);
    }

    public LoginLogoutException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoginLogoutException(Throwable cause) {
        super(cause);
    }
}
