package conference.utils.exceptions;

public class SelectionException extends Exception {
    public SelectionException() {
    }

    public SelectionException(String message) {
        super(message);
    }

    public SelectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public SelectionException(Throwable cause) {
        super(cause);
    }

    public SelectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
