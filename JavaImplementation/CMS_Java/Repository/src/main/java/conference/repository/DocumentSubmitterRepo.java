package conference.repository;

import conference.model.DocumentSubmitter;
import org.springframework.stereotype.Component;

@Component
public class DocumentSubmitterRepo extends Repository<DocumentSubmitter> {
    public DocumentSubmitterRepo() {
        super();
        setClassName(DocumentSubmitter.class);
    }
}