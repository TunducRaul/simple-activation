package conference.repository;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Component
public class PapersProvider {
    public PapersProvider() {
    }

    public String readFromFile(String fileName) {
        StringBuilder fileContent = new StringBuilder();
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);
            String currentLine = bufferedReader.readLine();
            while (currentLine != null) {
                fileContent.append(currentLine).append('\n');
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Cannot open file " + fileName);
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Cannot read file " + fileName);
            ex.printStackTrace();
        } finally {
            closeBuffer(bufferedReader);
            closeFileReader(fileReader);
        }
        return fileContent.toString();
    }

    private void closeBuffer(BufferedReader bufferedReader) {
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void closeFileReader(FileReader reader) {
        try {
            if (reader != null) {
                reader.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}