package conference.repository;

import conference.model.Chair;
import org.springframework.stereotype.Component;

@Component
public class ChairRepo extends Repository<Chair> {
    public ChairRepo() {
        super();
        setClassName(Chair.class);
    }
}