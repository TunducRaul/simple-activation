package conference.repository;

import conference.model.SteeringCommitteeMember;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

@Component
public class AdminRepo extends Repository<SteeringCommitteeMember> implements IAdminRepo {
    public AdminRepo() {
        super();
        setClassName(SteeringCommitteeMember.class);
    }

    @Override
    public SteeringCommitteeMember getAdminEntity(Session session, SteeringCommitteeMember steeringCommitteeMember) {
        String isAdmin = "isAdmin";
        return (SteeringCommitteeMember) session
                .createQuery("from " + className + " where " + username + "=:username and " +
                        password + "=:password" + " and " + isAdmin + "=true" + " and " + memberTypeString + "=:className")
                .setParameter("username", steeringCommitteeMember.getUsername())
                .setParameter("password", steeringCommitteeMember.getPassword())
                .setParameter("className", SteeringCommitteeMember.class.getName())
                .getSingleResult();
    }
}