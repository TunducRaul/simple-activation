package conference.repository.validators;

import conference.model.Deadline;
import conference.model.DeadlineType;
import conference.utils.ValidateException;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;

@Component
public class DeadlineValidator {

    public void validateNewDeadline(Deadline deadline) throws ValidateException {
        Date currentData = new Date();
        if (deadline.getDate().before(currentData)) {
            throw new ValidateException("You set a past date!");
        }
    }

    public void validateUpdateDeadline(Deadline newDeadline, Deadline oldDeadline, Set<Deadline> establishedDeadlines) throws ValidateException {
        validateNewDeadline(newDeadline);
        if (!haveSameType(newDeadline, oldDeadline) && deadlineAlreadySet(newDeadline, establishedDeadlines)) {
            throw new ValidateException("Deadline already set");
        }
    }

    private Deadline getDeadlineByType(Set<Deadline> deadlines, DeadlineType type) {
        Deadline foundDeadline = null;
        for (Deadline deadline : deadlines) {
            if (deadline.getType().equals(type)) {
                foundDeadline = deadline;
            }
        }
        return foundDeadline;
    }

    private Boolean isBeforeEstablishedDeadline(Deadline newDeadline, Deadline establishedDeadline) {
        return newDeadline.getDate().before(establishedDeadline.getDate());
    }

    private Boolean deadlineAlreadySet(Deadline deadline, Set<Deadline> establishedDeadlines) {
        Boolean found = false;
        Iterator<Deadline> iterator = establishedDeadlines.iterator();
        while (iterator.hasNext() && !found) {
            Deadline elem = iterator.next();
            if (elem.getType().equals(deadline.getType())) {
                found = true;
            }
        }
        return found;
    }

    private Boolean haveSameType(Deadline newDeadline, Deadline oldDeadline) {
        return newDeadline.getType().equals(oldDeadline.getType());
    }
}