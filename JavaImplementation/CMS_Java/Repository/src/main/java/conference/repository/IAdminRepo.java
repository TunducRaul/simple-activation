package conference.repository;

import conference.model.SteeringCommitteeMember;
import org.hibernate.Session;

public interface IAdminRepo{
    SteeringCommitteeMember getAdminEntity(Session session, SteeringCommitteeMember steeringCommitteeMember);
}