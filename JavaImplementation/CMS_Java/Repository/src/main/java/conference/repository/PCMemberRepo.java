package conference.repository;

import conference.model.PCMember;
import org.springframework.stereotype.Component;

@Component
public class PCMemberRepo extends Repository<PCMember> {
    public PCMemberRepo() {
        super();
        setClassName(PCMember.class);
    }
}