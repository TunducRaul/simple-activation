package conference.repository;

import conference.model.Deadline;
import org.springframework.stereotype.Component;

@Component
public class DeadlineRepo extends Repository<Deadline> {
    public DeadlineRepo() {
        super();
        setClassName(Deadline.class);
    }
}