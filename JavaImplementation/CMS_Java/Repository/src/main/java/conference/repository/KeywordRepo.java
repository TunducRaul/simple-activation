package conference.repository;

import conference.model.Keyword;
import org.springframework.stereotype.Component;

@Component
public class KeywordRepo extends Repository<Keyword> {
    public KeywordRepo() {
        super();
        setClassName(Keyword.class);
    }
}