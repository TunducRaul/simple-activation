package conference.repository;

import conference.model.SteeringCommitteeMember;
import org.springframework.stereotype.Component;

@Component
public class SCMemberRepo extends Repository<SteeringCommitteeMember> {
    public SCMemberRepo() {
        super();
        setClassName(SteeringCommitteeMember.class);
    }
}