package conference.repository;

import conference.model.Reviewer;
import org.springframework.stereotype.Component;

@Component
public class ReviewerRepo extends Repository<Reviewer> {
    public ReviewerRepo() {
        super();
        setClassName(Reviewer.class);
    }
}
