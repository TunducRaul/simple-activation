package conference.repository;

import conference.model.SteeringCommitteeMember;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.List;

public class Repository<Entity>{

    protected String className;
    protected final String username = "USERNAME";
    protected final String password = "PASSWORD";
    protected final String identifier = "id";
    protected final String packageName = "conference.model.";
    protected final String memberTypeString = "MEMBER_TYPE";

    public Repository() {
    }

    public Repository(Class<Entity> clazz) {
        String[] tokens = clazz.getName().split("\\.");
        this.className = tokens[tokens.length - 1];
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(Class<Entity> baseClass) {
        String[] tokens = baseClass.getName().split("\\.");
        this.className = tokens[tokens.length - 1];
    }

    public void save(Session session, Entity entity) {
        session.save(entity);
    }

    public void update(Session session, Entity entity) {
        session.update(entity);
    }

    public void delete(Session session, Entity entity) {
        session.delete(entity);
    }

    public Entity getById(Session session, Integer id) {
        Query query = session.createQuery("from " + className + " where " + identifier + "=:id")
                .setParameter("id", id);
        return ((Entity) query.getSingleResult());
    }

    public List<Entity> getAll(Session session) {
        return session
                .createQuery("from " + className)
                .list();
    }

    public List<Entity> getAllByMemberType(Session session) {
        return session
                .createQuery("from " + className + " where " + memberTypeString + "=:className")
                .setParameter("className", packageName + className)
                .list();
    }

    public Entity getLastRecord(Session session) {
        return (Entity) session
                .createQuery("from " + className + " order by " + identifier + " desc ")
                .setMaxResults(1)
                .uniqueResult();
    }

    public Entity getEntity(Session session, Entity entity) {
        SteeringCommitteeMember steeringCommitteeMember = (SteeringCommitteeMember) entity;
        return (Entity) session
                .createQuery("from " + className + " where " + username + "=:username and " + password + "=:password" +
                        " and " + memberTypeString + "=:className")
                .setParameter("username", steeringCommitteeMember.getUsername())
                .setParameter("password", steeringCommitteeMember.getPassword())
                .setParameter("className", packageName + className)
                .getSingleResult();
    }
}