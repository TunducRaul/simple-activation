package conference.repository;

import conference.model.Topic;
import org.springframework.stereotype.Component;

@Component
public class TopicRepo extends Repository<Topic> {
    public TopicRepo() {
        super();
        setClassName(Topic.class);
    }
}