package conference.repository.validators;

import conference.utils.ValidateException;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DateValidator {
    public void validateDate(Date date) throws ValidateException {
        Date currentDate = new Date();
        if (date.before(currentDate)) {
            throw new ValidateException("Before current date");
        }
    }
}