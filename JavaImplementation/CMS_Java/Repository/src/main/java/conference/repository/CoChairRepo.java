package conference.repository;

import conference.model.CoChair;
import org.springframework.stereotype.Component;

@Component
public class CoChairRepo extends Repository<CoChair> {
    public CoChairRepo() {
        super();
        setClassName(CoChair.class);
    }
}