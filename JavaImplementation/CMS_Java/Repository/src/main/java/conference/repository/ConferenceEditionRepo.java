package conference.repository;

import conference.model.ConferenceEdition;
import org.springframework.stereotype.Component;

@Component
public class ConferenceEditionRepo extends Repository<ConferenceEdition> {
    public ConferenceEditionRepo() {
        super();
        setClassName(ConferenceEdition.class);
    }
}