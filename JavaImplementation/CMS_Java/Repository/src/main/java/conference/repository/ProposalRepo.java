package conference.repository;

import conference.model.Proposal;
import org.springframework.stereotype.Component;

@Component
public class ProposalRepo extends Repository<Proposal> {
    public ProposalRepo() {
        super();
        setClassName(Proposal.class);
    }
}