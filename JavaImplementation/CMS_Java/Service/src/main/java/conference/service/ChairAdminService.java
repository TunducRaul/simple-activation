package conference.service;

import conference.model.Chair;
import conference.model.Deadline;
import conference.model.PCMember;
import conference.repository.ChairRepo;
import conference.repository.DeadlineRepo;
import conference.repository.PCMemberRepo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class ChairAdminService {
    @Autowired
    private ChairRepo chairRepository;
    @Autowired
    private PCMemberRepo pcMemberRepository;
    @Autowired
    private DeadlineRepo deadlineRepository;
    @Autowired
    private SessionFactory sessionFactory;
    private Logger logger = Logger.getLogger(getClass().getName());

    public ChairAdminService() {
    }

    public ChairRepo getChairRepository() {
        return chairRepository;
    }

    public void setChairRepository(ChairRepo chairRepository) {
        this.chairRepository = chairRepository;
    }

    public PCMemberRepo getPcMemberRepository() {
        return pcMemberRepository;
    }

    public void setPcMemberRepository(PCMemberRepo pcMemberRepository) {
        this.pcMemberRepository = pcMemberRepository;
    }

    public DeadlineRepo getDeadlineRepository() {
        return deadlineRepository;
    }

    public void setDeadlineRepository(DeadlineRepo deadlineRepository) {
        this.deadlineRepository = deadlineRepository;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void registerChairAdmin(String username, String password) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            PCMember member = new PCMember();
            member.setUsername(username);
            member.setPassword(password);
            member = pcMemberRepository.getEntity(session, member);
            if (member == null)
                throw new Exception("PCMember doesn't exist");
            Chair chair = new Chair();
            chair.setIsAdmin(true);
            chair.setFirstName(member.getFirstName());
            chair.setLastName(member.getLastName());
            chair.setUsername(member.getUsername());
            chair.setPassword(member.getPassword());
            chair.setAffiliation(member.getAffiliation());
            chair.setEmailAddress(member.getEmailAddress());
            chair.setWebPage(member.getWebPage());
            chairRepository.save(session, chair);
            transaction.commit();
        } catch (Exception e) {
            logger.warning(e.getMessage());
            rollBackTran(transaction);
        } finally {
            closeSession(session);
        }
    }

    public void changeDeadline(Deadline deadline) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Deadline oldDeadline = deadlineRepository.getById(session, deadline.getDeadlineId());
            if (deadline.getDate().before(oldDeadline.getDate()))
                throw new Exception("Invalid deadline!");
            deadlineRepository.update(session, deadline);
            transaction.commit();
        } catch (Exception e) {
            logger.warning(e.getMessage());
            rollBackTran(transaction);
        } finally {
            closeSession(session);
        }
    }

    private void rollBackTran(Transaction transaction) {
        if (transaction != null) {
            transaction.rollback();
        }
    }

    private void closeSession(Session session) {
        if (session != null) {
            session.close();
        }
    }
}
