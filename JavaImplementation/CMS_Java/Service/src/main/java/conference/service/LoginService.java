package conference.service;

import conference.model.*;
import conference.repository.*;
import conference.utils.BUSNotifier;
import conference.utils.exceptions.LoginLogoutException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.NoResultException;
import java.util.Objects;
import java.util.logging.Logger;

@Component
public class LoginService {
    @Autowired
    private IAdminRepo adminRepo;
    @Autowired
    private DocumentSubmitterRepo submitterRepo;
    @Autowired
    private PCMemberRepo pcMemberRepo;
    @Autowired
    private ChairRepo chairRepo;
    @Autowired
    private CoChairRepo coChairRepo;
    @Autowired
    private ReviewerRepo reviewerRepo;
    @Autowired
    private ProposalRepo proposalRepo;
    @Autowired
    private ConferenceEditionRepo conferenceEditionRepo;
    @Autowired
    private SessionFactory sessionFactory;
    private Logger logger = Logger.getLogger(getClass().getName());

    public LoginService() {
    }

    public IAdminRepo getAdminRepo() {
        return adminRepo;
    }

    public void setAdminRepo(IAdminRepo adminRepo) {
        this.adminRepo = adminRepo;
    }

    public DocumentSubmitterRepo getSubmitterRepo() {
        return submitterRepo;
    }

    public void setSubmitterRepo(DocumentSubmitterRepo submitterRepo) {
        this.submitterRepo = submitterRepo;
    }

    public PCMemberRepo getPcMemberRepo() {
        return pcMemberRepo;
    }

    public void setPcMemberRepo(PCMemberRepo pcMemberRepo) {
        this.pcMemberRepo = pcMemberRepo;
    }

    public ChairRepo getChairRepo() {
        return chairRepo;
    }

    public void setChairRepo(ChairRepo chairRepo) {
        this.chairRepo = chairRepo;
    }

    public CoChairRepo getCoChairRepo() {
        return coChairRepo;
    }

    public void setCoChairRepo(CoChairRepo coChairRepo) {
        this.coChairRepo = coChairRepo;
    }

    public ReviewerRepo getReviewerRepo() {
        return reviewerRepo;
    }

    public void setReviewerRepo(ReviewerRepo reviewerRepo) {
        this.reviewerRepo = reviewerRepo;
    }

    public ProposalRepo getProposalRepo() {
        return proposalRepo;
    }

    public void setProposalRepo(ProposalRepo proposalRepo) {
        this.proposalRepo = proposalRepo;
    }

    public ConferenceEditionRepo getConferenceEditionRepo() {
        return conferenceEditionRepo;
    }

    public void setConferenceEditionRepo(ConferenceEditionRepo conferenceEditionRepo) {
        this.conferenceEditionRepo = conferenceEditionRepo;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private ConferenceEdition getCurrentEdition(Session session) {
        ConferenceEdition edition;
        edition = conferenceEditionRepo.getLastRecord(session);
        return edition;
    }

    public SteeringCommitteeMember loginAdmin(String username, String password) throws LoginLogoutException {
        Session session = null;
        Transaction transaction = null;
        SteeringCommitteeMember admin;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            SteeringCommitteeMember scm = new SteeringCommitteeMember();
            scm.setUsername(username);
            scm.setPassword(password);
            admin = adminRepo.getAdminEntity(session, scm);
            transaction.commit();
        } catch (NoResultException ex) {
            rollBackTran(transaction);
            logger.warning("[LoginService] login error: " + ex.getMessage());
            throw new LoginLogoutException("Wrong username or password");
        } finally {
            closeSession(session);
        }
        return admin;
    }

    public IAuthor loginAuthor(String username, String password) throws LoginLogoutException {
        Session session = null;
        Transaction transaction = null;
        IAuthor author;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            DocumentSubmitter documentSubmitter = new DocumentSubmitter();
            documentSubmitter.setUsername(username);
            documentSubmitter.setPassword(password);
            author = submitterRepo.getEntity(session, documentSubmitter);
            transaction.commit();
        } catch (NoResultException ex) {
            rollBackTran(transaction);
            logger.warning("[LoginService] login error: " + ex.getMessage());
            throw new LoginLogoutException("Wrong username or password");
        } finally {
            closeSession(session);
        }
        return author;
    }

    public ISpeaker loginSpeaker(String username, String password) throws LoginLogoutException {
        Session session = null;
        Transaction transaction = null;
        ISpeaker speaker;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition currentEdition = getCurrentEdition(session);
            DocumentSubmitter documentSubmitter = new DocumentSubmitter();
            documentSubmitter.setUsername(username);
            documentSubmitter.setPassword(password);
            speaker = submitterRepo.getEntity(session, documentSubmitter);
            transaction.commit();
            if (currentEdition == null || !currentEdition.existSpeaker(speaker)) {
                throw new LoginLogoutException("You're not Speaker at the current Conference Edition");
            }
        } catch (NoResultException ex) {
            rollBackTran(transaction);
            logger.warning("[LoginService] login error: " + ex.getMessage());
            throw new LoginLogoutException("Wrong username or password");
        } finally {
            closeSession(session);
        }
        return speaker;
    }

    public Chair loginChair(String username, String password) throws LoginLogoutException {
        Session session = null;
        Transaction transaction = null;
        Chair result;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition currentEdition = getCurrentEdition(session);
            Chair chair = new Chair();
            chair.setUsername(username);
            chair.setPassword(password);
            result = chairRepo.getEntity(session, chair);
            transaction.commit();
            if (currentEdition == null || !currentEdition.getChair().equals(result)) {
                throw new LoginLogoutException("You're not Chair at the current Conference Edition");
            }
        } catch (NoResultException ex) {
            rollBackTran(transaction);
            logger.warning("[LoginService] login error: " + ex.getMessage());
            throw new LoginLogoutException("Wrong username or password");
        } finally {
            closeSession(session);
        }
        return result;
    }

    public Boolean isChairOrCoChairAdmin(Integer chairCoChairId) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition currentEdition = getCurrentEdition(session);
            transaction.commit();
            if (currentEdition != null) {
                Integer conferenceAdminId = currentEdition.getCurrentAdminId();
                return Objects.equals(chairCoChairId, conferenceAdminId);
            }
        } catch (Exception ex) {
            rollBackTran(transaction);
            BUSNotifier.error("Cannot provide Chair");
        } finally {
            closeSession(session);
        }
        return false;
    }

    public CoChair loginCoChair(String username, String password) throws LoginLogoutException {
        Session session = null;
        CoChair result;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition currentEdition = getCurrentEdition(session);
            CoChair coChair = new CoChair();
            coChair.setUsername(username);
            coChair.setPassword(password);
            result = coChairRepo.getEntity(session, coChair);
            transaction.commit();
            if (currentEdition == null || !currentEdition.existCoChair(result)) {
                throw new LoginLogoutException("You're not CoChair at the current Conference Edition");
            }
        } catch (NoResultException ex) {
            rollBackTran(transaction);
            logger.warning("[LoginService] login error: " + ex.getMessage());
            throw new LoginLogoutException("Wrong username or password");
        } finally {
            closeSession(session);
        }
        return result;
    }

    public IListener loginListener(String username, String password) throws LoginLogoutException {
        Session session = null;
        IListener result;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition currentEdition = getCurrentEdition(session);
            PCMember listener = new PCMember();
            listener.setUsername(username);
            listener.setPassword(password);
            result = pcMemberRepo.getEntity(session, listener);
            transaction.commit();
            if (currentEdition == null || !currentEdition.existListener(listener)) {
                throw new LoginLogoutException("You're not listener at the current Conference Edition");
            }
        } catch (NoResultException ex) {
            rollBackTran(transaction);
            logger.warning("[LoginService] login error: " + ex.getMessage());
            throw new LoginLogoutException("Wrong username or password");
        } finally {
            closeSession(session);
        }
        return result;
    }

    public Reviewer loginReviewer(String username, String password) throws LoginLogoutException {
        Session session = null;
        Reviewer result;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition currentEdition = getCurrentEdition(session);
            Reviewer reviewer = new Reviewer();
            reviewer.setUsername(username);
            reviewer.setPassword(password);
            result = reviewerRepo.getEntity(session, reviewer);
            transaction.commit();
            if (currentEdition == null || !currentEdition.existReviewer(reviewer)) {
                throw new LoginLogoutException("You're not reviewer at the current Conference Edition");
            }
        } catch (NoResultException ex) {
            rollBackTran(transaction);
            logger.warning("[LoginService] login error: " + ex.getMessage());
            throw new LoginLogoutException("Wrong username or password");
        } finally {
            closeSession(session);
        }
        return result;
    }

    public PCMember loginPCMember(String username, String password) throws LoginLogoutException {
        Session session = null;
        PCMember result;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition currentEdition = getCurrentEdition(session);
            PCMember pcMember = new PCMember();
            pcMember.setUsername(username);
            pcMember.setPassword(password);
            result = pcMemberRepo.getEntity(session, pcMember);
            transaction.commit();
            if (currentEdition == null || !currentEdition.existPCMember(pcMember)) {
                throw new LoginLogoutException("You're not PCMember at the current Conference Edition");
            }
        } catch (NoResultException ex) {
            rollBackTran(transaction);
            logger.warning("[LoginService] login error: " + ex.getMessage());
            throw new LoginLogoutException("Wrong username or password");
        } finally {
            closeSession(session);
        }
        return result;
    }

    private void closeSession(Session session) {
        if (session != null) {
            session.close();
        }
    }

    private void rollBackTran(Transaction transaction) {
        if (transaction != null) {
            transaction.rollback();
        }
    }

    public void logout() throws HibernateException {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }
}