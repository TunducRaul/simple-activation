package conference.service;

import conference.model.*;
import conference.model.Keyword;
import conference.model.Proposal;
import conference.model.Topic;
import conference.repository.*;
import conference.utils.exceptions.RegisterException;
import conference.utils.exceptions.UploadException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.NoResultException;
import java.util.*;
import java.util.logging.Logger;

@Component
public class AuthorService {
    @Autowired
    private DocumentSubmitterRepo submitterRepo;
    @Autowired
    private ConferenceEditionRepo conferenceEditionRepo;
    @Autowired
    private SCMemberRepo steeringCommitteeMemberRepository;
    @Autowired
    private ReviewerRepo reviewerRepository;
    @Autowired
    private ProposalRepo proposalRepository;
    @Autowired
    private TopicRepo topicRepository;
    @Autowired
    private KeywordRepo keywordRepository;
    @Autowired
    private SessionFactory sessionFactory;
    private Logger logger = Logger.getLogger(getClass().getName());


    public ConferenceEdition getCurrentEdition() {
        Session session = null;
        ConferenceEdition edition = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            edition = conferenceEditionRepo.getLastRecord(session);
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
        } finally {
            closeSession(session);
        }
        return edition;
    }

    public void registerAuthor(String affiliation, String email, String username, String password) throws RegisterException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            SteeringCommitteeMember steeringCommitteeMember = new SteeringCommitteeMember();
            steeringCommitteeMember.setUsername(username);
            steeringCommitteeMember.setPassword(password);
            SteeringCommitteeMember foundMember = steeringCommitteeMemberRepository.getEntity(session, steeringCommitteeMember);
            DocumentSubmitter documentSubmitter = new DocumentSubmitter();
            documentSubmitter.setUsername(username);
            documentSubmitter.setPassword(password);
            documentSubmitter.setAffiliation(affiliation);
            documentSubmitter.setEmailAddress(email);
            documentSubmitter.setLastName(foundMember.getLastName());
            documentSubmitter.setFirstName(foundMember.getFirstName());
            documentSubmitter.setId(foundMember.getId());
            Reviewer reviewer = new Reviewer();
            reviewer.setUsername(username);
            reviewer.setPassword(password);
            try {
                reviewerRepository.getEntity(session, reviewer);
                documentSubmitter.setIsReviewer(true);
            } catch (NoResultException ex) {
                documentSubmitter.setIsReviewer(false);
            }
            submitterRepo.save(session, documentSubmitter);
            transaction.commit();
        } catch (NoResultException ex) {
            rollBackTran(transaction);
            throw new RegisterException("Entity was not found in database!");
        } finally {
            closeSession(session);
        }
    }

    public void uploadPaper(String title, String topics, String keywords, String abstractPaperPath,
                            String fullPaperPath, DocumentSubmitter author) throws UploadException {
        Session session = null;
        Transaction transaction = null;
        Proposal proposal = new Proposal();
        ConferenceEdition conferenceEdition = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            conferenceEdition = conferenceEditionRepo.getLastRecord(session);
            IAuthor owner = submitterRepo.getById(session, author.getId());
            proposal.setOwner(owner);
            proposal.setFullPaperLocation(fullPaperPath);
            proposal.setAbstractPaperLocation(abstractPaperPath);
            proposal.setTitle(title);
            proposal.setProposalID(author.getId());
            proposal.setConferenceEdition(conferenceEdition);
            proposalRepository.save(session, proposal);
            List<String> topicList = Arrays.asList(topics.split(";"));
            for (String t : topicList) {
                Topic newTopic = new Topic();
                newTopic.setMessage(t);
                newTopic.setProposal(proposal);
                topicRepository.save(session, newTopic);
                newTopic = topicRepository.getLastRecord(session);
                proposal.addTopic(newTopic);
            }
            List<String> keywordsList = Arrays.asList(keywords.split(";"));
            for (String k : keywordsList) {
                Keyword newKeyword = new Keyword();
                newKeyword.setMessage(k);
                newKeyword.setProposal(proposal);
                keywordRepository.save(session, newKeyword);
                newKeyword = keywordRepository.getLastRecord(session);
                proposal.addKeyword(newKeyword);
            }
            proposalRepository.update(session, proposal);
            proposal = proposalRepository.getLastRecord(session);
            conferenceEdition.addProposal(proposal);
            conferenceEditionRepo.update(session, conferenceEdition);
            transaction.commit();
        } catch (Exception e) {
            rollBackTran(transaction);
            e.printStackTrace();
            throw new UploadException(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    public Set<ProposalReviewer> getAllRecommendations(DocumentSubmitter documentSubmitter) {
        Session session = null;
        Transaction transaction = null;
        Set<ProposalReviewer> proposalReviewerSet = new HashSet<>();
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Proposal proposal = proposalRepository.getById(session, documentSubmitter.getId());
            proposalReviewerSet = proposal.getProposalReviewer();
            transaction.commit();
        } catch (Exception e) {
            rollBackTran(transaction);
            e.printStackTrace();
        } finally {
            closeSession(session);
        }
        return proposalReviewerSet;
    }

    private void rollBackTran(Transaction transaction) {
        if (transaction != null) {
            transaction.rollback();
        }
    }

    private void closeSession(Session session) {
        if (session != null) {
            session.close();
        }
    }
}