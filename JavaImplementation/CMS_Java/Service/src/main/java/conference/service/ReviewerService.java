package conference.service;

import conference.model.*;
import conference.model.Proposal;
import conference.repository.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.logging.Logger;

@Component
public class ReviewerService {
    @Autowired
    private SCMemberRepo steeringCommitteeMemberRepository;
    @Autowired
    private ReviewerRepo reviewerRepository;
    @Autowired
    private ProposalRepo proposalRepository;
    @Autowired
    private ConferenceEditionRepo conferenceEditionRepo;
    @Autowired
    private SessionFactory sessionFactory;
    private Logger logger = Logger.getLogger(getClass().getName());

    public ReviewerService() {
    }

    public SCMemberRepo getSteeringCommitteeMemberRepository() {
        return steeringCommitteeMemberRepository;
    }

    public void setSteeringCommitteeMemberRepository(SCMemberRepo steeringCommitteeMemberRepository) {
        this.steeringCommitteeMemberRepository = steeringCommitteeMemberRepository;
    }

    public ReviewerRepo getReviewerRepository() {
        return reviewerRepository;
    }

    public void setReviewerRepository(ReviewerRepo reviewerRepository) {
        this.reviewerRepository = reviewerRepository;
    }

    public ProposalRepo getProposalRepository() {
        return proposalRepository;
    }

    public void setProposalRepository(ProposalRepo proposalRepository) {
        this.proposalRepository = proposalRepository;
    }

    public ConferenceEditionRepo getConferenceEditionRepo() {
        return conferenceEditionRepo;
    }

    public void setConferenceEditionRepo(ConferenceEditionRepo conferenceEditionRepo) {
        this.conferenceEditionRepo = conferenceEditionRepo;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Proposal> getProposalsToBeEvaluated(Reviewer reviewer) {
        Session session = null;
        Transaction transaction = null;
        List<Proposal> proposalsToBeEvaluated = new ArrayList<>();
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            List<Proposal> proposalList = proposalRepository.getAll(session);
            transaction.commit();
            for (Proposal proposal : proposalList) {
                Set<ProposalReviewer> proposalReviewers = proposal.getProposalReviewer();
                Iterator<ProposalReviewer> iterator = proposalReviewers.iterator();
                Boolean found = false;
                while (iterator.hasNext() && !found) {
                    ProposalReviewer proposalReviewer = iterator.next();
                    if (proposalReviewer.getId().getReviewer().equals(reviewer)) {
                        proposalsToBeEvaluated.add(proposal);
                        found = true;
                    }
                }
            }
        } catch (Exception ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
        } finally {
            closeSession(session);
        }
        return proposalsToBeEvaluated;
    }

    private void rollBackTran(Transaction transaction) {
        if (transaction != null) {
            transaction.rollback();
        }
    }

    private void closeSession(Session session) {
        if (session != null) {
            session.close();
        }
    }
}