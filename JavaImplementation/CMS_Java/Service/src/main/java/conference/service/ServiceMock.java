package conference.service;

import conference.model.*;
import conference.repository.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

@Component
public class ServiceMock {
    @Autowired
    private ChairRepo chairRepo;
    @Autowired
    private CoChairRepo coChairRepo;
    @Autowired
    private ConferenceEditionRepo conferenceEditionRepo;
    @Autowired
    private DeadlineRepo deadlineRepo;
    @Autowired
    private DocumentSubmitterRepo documentSubmitterRepo;
    @Autowired
    private IAdminRepo adminRepol;
    @Autowired
    private KeywordRepo keywordRepol;
    @Autowired
    private PapersProvider papersProvider;
    @Autowired
    private PCMemberRepo pcMemberRepo;
    @Autowired
    private ProposalRepo proposalRepo;
    @Autowired
    private ReviewerRepo reviewerRepo;
    @Autowired
    private SCMemberRepo scMemberRepo;
    @Autowired
    private TopicRepo topicRepo;
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private AdminService adminService;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public ServiceMock() {
    }

    private void rollbackTran(Transaction transaction) {
        if (transaction != null) {
            transaction.rollback();
        }
    }

    private void closeSession(Session session) {
        if (session != null) {
            session.close();
        }
    }

    public void addMockData() {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            // Add steering committee members
            SteeringCommitteeMember SCMember = new SteeringCommitteeMember("firstName", "lastName",
                    -1, true, "admin", "admin"
            );
            SteeringCommitteeMember SCMemberSecond = new SteeringCommitteeMember("secondFN",
                    "secondLN", -2, false, "user", "password"
            );
            SteeringCommitteeMember SCMemberThird = new SteeringCommitteeMember("thirdFN",
                    "thirdLN", -3, false, "ana", "maria"
            );
            scMemberRepo.save(session, SCMember);
            SCMember = scMemberRepo.getLastRecord(session);
            scMemberRepo.save(session, SCMemberSecond);
            SCMemberSecond = scMemberRepo.getLastRecord(session);
            scMemberRepo.save(session, SCMemberThird);
            SCMemberThird = scMemberRepo.getLastRecord(session);
            // Add mock PC Members
            PCMember pcMember = new PCMember(SCMember.getFirstName(), SCMember.getLastName(), SCMember.getId(),
                    SCMember.getIsAdmin(), SCMember.getUsername(), SCMember.getPassword(), "affiliation",
                    "email address", "web page"
            );
            pcMemberRepo.save(session, pcMember);
            pcMember = pcMemberRepo.getLastRecord(session);
            PCMember pcMemberSecond = new PCMember(SCMemberSecond.getFirstName(), SCMemberSecond.getLastName(),
                    SCMemberSecond.getId(), SCMemberSecond.getIsAdmin(), SCMemberSecond.getUsername(),
                    SCMemberSecond.getPassword(), "Second affliliation", "second email",
                    "second web page"
            );
            pcMemberRepo.save(session, pcMemberSecond);
            pcMemberSecond = pcMemberRepo.getLastRecord(session);
            PCMember pcMemberThird = new PCMember(SCMemberThird.getFirstName(), SCMemberThird.getLastName(),
                    SCMemberThird.getId(), SCMemberThird.getIsAdmin(), SCMemberThird.getUsername(),
                    SCMemberThird.getPassword(), "third aff", "third email", "third page"
            );
            pcMemberRepo.save(session, pcMember);
            pcMemberThird = pcMemberRepo.getLastRecord(session);
            // Add reviewer
            Reviewer reviewer = new Reviewer();
            reviewer.setFirstName(pcMember.getFirstName());
            reviewer.setLastName(pcMember.getLastName());
            reviewer.setId(pcMember.getId());
            reviewer.setUsername(pcMember.getUsername());
            reviewer.setPassword(pcMember.getPassword());
            reviewer.setIsAdmin(pcMember.getIsAdmin());
            reviewer.setAffiliation(pcMember.getAffiliation());
            reviewer.setEmailAddress(pcMember.getEmailAddress());
            reviewer.setWebPage(pcMember.getWebPage());
            reviewerRepo.save(session, reviewer);
            reviewer = reviewerRepo.getLastRecord(session);
            // Add chair
            Chair chair = new Chair();
            chair.setId(pcMemberSecond.getId());
            chair.setUsername(pcMemberSecond.getUsername());
            chair.setPassword(pcMemberSecond.getPassword());
            chair.setFirstName(pcMemberSecond.getFirstName());
            chair.setLastName(pcMemberSecond.getLastName());
            chair.setIsAdmin(pcMemberSecond.getIsAdmin());
            chair.setAffiliation(pcMemberSecond.getAffiliation());
            chair.setEmailAddress(pcMemberSecond.getEmailAddress());
            chair.setWebPage(pcMemberSecond.getWebPage());
            chairRepo.save(session, chair);
            chair = chairRepo.getLastRecord(session);
            // Add CoChair
            CoChair coChair = new CoChair();
            coChair.setId(pcMemberThird.getId());
            coChair.setUsername(pcMemberThird.getUsername());
            coChair.setPassword(pcMemberThird.getPassword());
            coChair.setFirstName(pcMemberThird.getFirstName());
            coChair.setLastName(pcMemberThird.getLastName());
            coChair.setIsAdmin(pcMemberThird.getIsAdmin());
            coChair.setAffiliation(pcMemberThird.getAffiliation());
            coChair.setEmailAddress(pcMemberThird.getEmailAddress());
            coChair.setWebPage(pcMemberThird.getWebPage());
            coChairRepo.save(session, coChair);
            coChair = coChairRepo.getLastRecord(session);
            // Add conference edition
            ConferenceEdition conferenceEdition = new ConferenceEdition();
            conferenceEdition.setName("Conference Edition Name");
            conferenceEdition.setStartDate(dateFormat.parse("02/02/2018"));
            conferenceEdition.setStopDate(dateFormat.parse("03/03/2018"));
            conferenceEdition.setCanUpload(false);
            conferenceEdition.setChair(chair);
            conferenceEdition.setCurrentAdminId(1);
            conferenceEditionRepo.save(session, conferenceEdition);
            conferenceEdition = conferenceEditionRepo.getLastRecord(session);
            // Add deadlines
            Deadline biddingDeadline = new Deadline();
            biddingDeadline.setDate(dateFormat.parse("05/02/2018"));
            biddingDeadline.setType(DeadlineType.BIDDING);
            deadlineRepo.save(session, biddingDeadline);
            biddingDeadline = deadlineRepo.getLastRecord(session);
            Deadline abstractPaperDeadline = new Deadline();
            abstractPaperDeadline.setDate(dateFormat.parse("25/02/2018"));
            abstractPaperDeadline.setType(DeadlineType.UPLOAD_ABSTRACT_PAPER);
            deadlineRepo.save(session, abstractPaperDeadline);
            abstractPaperDeadline = deadlineRepo.getLastRecord(session);
            Deadline callForPapersDeadline = new Deadline();
            callForPapersDeadline.setDate(dateFormat.parse("29/02/2018"));
            callForPapersDeadline.setType(DeadlineType.CALL_FOR_PAPERS);
            deadlineRepo.save(session, callForPapersDeadline);
            callForPapersDeadline = deadlineRepo.getLastRecord(session);
            Set<Deadline> deadlineSet = new HashSet<>();
            deadlineSet.add(biddingDeadline);
            deadlineSet.add(abstractPaperDeadline);
            deadlineSet.add(callForPapersDeadline);
            conferenceEdition.setDeadlines(deadlineSet);
            conferenceEditionRepo.update(session, conferenceEdition);
            transaction.commit();
        } catch (Exception ex) {
            rollbackTran(transaction);
            ex.printStackTrace();
        } finally {
            closeSession(session);
        }
    }
}