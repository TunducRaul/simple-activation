package conference.service;

import conference.model.*;
import conference.model.Proposal;
import conference.repository.*;
import conference.repository.validators.DateValidator;
import conference.repository.validators.DeadlineValidator;
import conference.utils.exceptions.DeadlineException;
import conference.utils.exceptions.ProviderException;
import conference.utils.exceptions.RegisterException;
import conference.utils.ValidateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

@Component
public class AdminService {
    @Autowired
    private SCMemberRepo SCMembersRepo;
    @Autowired
    private PCMemberRepo pcMemberRepo;
    @Autowired
    private ChairRepo chairRepo;
    @Autowired
    private CoChairRepo coChairRepo;
    @Autowired
    private ReviewerRepo reviewerRepo;
    @Autowired
    private ProposalRepo proposalRepo;
    @Autowired
    private ConferenceEditionRepo conferenceEditionRepo;
    @Autowired
    private DeadlineRepo deadlineRepo;
    @Autowired
    private SessionFactory sessionFactory;
    private Logger logger = Logger.getLogger(getClass().getName());
    @Autowired
    private DeadlineValidator deadlineValidator;
    @Autowired
    private DateValidator dateValidator;

    public AdminService() {
    }

    private Boolean checkUsers(SteeringCommitteeMember firstSCMember, SteeringCommitteeMember secondSCMember) {
        return firstSCMember.getUsername().equals(secondSCMember.getUsername()) &&
                firstSCMember.getPassword().equals(secondSCMember.getPassword());
    }

    private void rollBackTran(Transaction transaction) {
        if (transaction != null) {
            transaction.rollback();
        }
    }

    private ConferenceEdition getCurrentEdition(Session session) {
        ConferenceEdition edition;
        edition = conferenceEditionRepo.getLastRecord(session);
        return edition;
    }

    public Set<Deadline> getAllDeadlines() {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition edition = getCurrentEdition(session);
            transaction.commit();
            return edition.getDeadlines();
        } catch (Exception ex) {
            rollBackTran(transaction);
        } finally {
            closeSession(session);
        }
        return null;
    }

    public List<ConferenceEdition> getAllConferenceEditions() throws ProviderException {
        Session session = null;
        List<ConferenceEdition> conferenceEditions;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            conferenceEditions = conferenceEditionRepo.getAll(session);
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw new ProviderException("Cannot provide conference editions");
        } finally {
            closeSession(session);
        }
        return conferenceEditions;
    }

    public List<PCMember> getAllPCMembers() throws ProviderException {
        Session session = null;
        List<PCMember> pcMembers = new ArrayList<>();
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            pcMembers = pcMemberRepo.getAllByMemberType(session);
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw new ProviderException("Cannot provide pcMembers");
        } finally {
            closeSession(session);
        }
        return pcMembers;
    }

    public List<SteeringCommitteeMember> getAllSCMembers() throws ProviderException {
        Session session = null;
        List<SteeringCommitteeMember> SCMembers;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            SCMembers = SCMembersRepo.getAllByMemberType(session);
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw new ProviderException("Cannot provide Steering Committee Members");
        } finally {
            closeSession(session);
        }
        return SCMembers;
    }

    public void addDeadline(Date date, DeadlineType deadlineType) throws DeadlineException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition currentEdition = getCurrentEdition(session);
            Deadline deadline = new Deadline();
            deadline.setDate(date);
            deadline.setType(deadlineType);
            deadlineValidator.validateNewDeadline(deadline);
            deadlineRepo.save(session, deadline);
            deadline = deadlineRepo.getLastRecord(session);
            currentEdition.addDeadLine(deadline);
            conferenceEditionRepo.update(session, currentEdition);
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw new DeadlineException("Cannot add deadline");
        } finally {
            closeSession(session);
        }
    }

    public void updateDeadline(Date newDate, DeadlineType newDeadlineType, Deadline deadline) throws ProviderException, ValidateException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition currentConference = conferenceEditionRepo.getLastRecord(session);
            deadline = deadlineRepo.getById(session, deadline.getDeadlineId());
            Deadline auxDeadline = new Deadline();
            auxDeadline.setDate(newDate);
            auxDeadline.setType(newDeadlineType);
            deadlineValidator.validateUpdateDeadline(auxDeadline, deadline, currentConference.getDeadlines());
            deadline.setDate(newDate);
            deadline.setType(newDeadlineType);
            deadlineRepo.update(session, deadline);
            transaction.commit();
        } catch (ValidateException ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw ex;
        } catch (Exception ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw new ProviderException("Cannot modify deadline");
        } finally {
            closeSession(session);
        }
    }

    public void saveConferenceEdition(String name, String startDateString, String endDateString, List<PCMember> pcMembers,
                                      Chair chair, Set<CoChair> coChairList, Set<Reviewer> reviewerList, Boolean canUpload,
                                      Integer conferenceAdminId)
            throws ProviderException, ValidateException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            chairRepo.save(session, chair);
            for (PCMember pcMember : pcMembers) {
                pcMemberRepo.save(session, pcMember);
            }
            for (CoChair coChair : coChairList) {
                coChairRepo.save(session, coChair);
            }
            for (Reviewer reviewer : reviewerList) {
                reviewerRepo.save(session, reviewer);
            }

            ConferenceEdition conferenceEdition = new ConferenceEdition();
            conferenceEdition.setId(chair.getId());
            conferenceEdition.setName(name);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = dateFormat.parse(startDateString);
            dateValidator.validateDate(startDate);
            Date endDate = dateFormat.parse(endDateString);
            dateValidator.validateDate(endDate);
            conferenceEdition.setStartDate(startDate);
            conferenceEdition.setStopDate(endDate);
            conferenceEdition.setCurrentAdminId(conferenceAdminId);
            conferenceEdition.setChair(chair);
            conferenceEdition.setCoChairs(coChairList);
            conferenceEdition.setReviewers(reviewerList);
            conferenceEdition.setCanUpload(canUpload);
            conferenceEdition.setPcMembers(new HashSet<>(pcMembers));
            conferenceEditionRepo.save(session, conferenceEdition);
            transaction.commit();

        } catch (ValidateException ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw ex;
        } catch (Exception ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw new ProviderException("Cannot add conference edition");
        } finally {
            closeSession(session);
        }
    }

    public void removeConferenceEdition(ConferenceEdition conferenceEdition) throws ProviderException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            conferenceEditionRepo.delete(session, conferenceEdition);
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw new ProviderException("Cannot remove conference");
        } finally {
            closeSession(session);
        }
    }

    public List<Proposal> getAllProposals() throws ProviderException {
        Session session = null;
        List<Proposal> proposals;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            proposals = proposalRepo.getAll(session);
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
            ex.printStackTrace();
            throw new ProviderException("Cannot provide proposals");
        } finally {
            closeSession(session);
        }
        return proposals;
    }

    public List<Reviewer> getAllReviewers() throws ProviderException {
        logger.info("[AdminService] Get all reviewers");
        List<Reviewer> reviewers;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            reviewers = reviewerRepo.getAll(session);
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
            throw new ProviderException("Cannot provide reviewers");
        } finally {
            closeSession(session);
        }
        return reviewers;
    }

    public List<Proposal> getProposalsAfterBiddingProcess() throws ProviderException {
        logger.info("[AdminService] Provide Accepted proposals after bidding process");
        List<Proposal> proposals;
        List<Proposal> refusedProposals = new ArrayList<>();
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            proposals = proposalRepo.getAll(session);
            proposals.forEach(proposal -> {
                if (proposal.isRefusedInBiddingPhase()) {
                    refusedProposals.add(proposal);
                }
            });
            if (refusedProposals.size() > 0) {
                removeRefusedProposals(session, refusedProposals);
                proposals = proposalRepo.getAll(session);
            }
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
            throw new ProviderException("Cannot provide ProposalPcMembers | Cannot remove refused proposals");
        } finally {
            closeSession(session);
        }
        return proposals;
    }

    private void removeRefusedProposals(Session session, List<Proposal> refusedProposals) {
        refusedProposals.forEach(refusedProposal -> proposalRepo.delete(session, refusedProposal));
    }

    public Set<IAuthor> getCurrentEditionAuthors() throws ProviderException {
        Session session = null;
        Transaction transaction = null;
        Set<IAuthor> authors = new HashSet<>();
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ConferenceEdition conferenceEdition = getCurrentEdition(session);
            transaction.commit();
            if (conferenceEdition != null) {
                return conferenceEdition.getAuthors();
            }
        } catch (Exception ex) {
            rollBackTran(transaction);
            throw new ProviderException("Cannot provide authors");
        } finally {
            closeSession(session);
        }
        return authors;
    }

    public void saveCurrentPCMembers(Set<PCMember> pcMembersList) throws RegisterException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            for (PCMember pcMember : pcMembersList) {
                pcMemberRepo.save(session, pcMember);
            }
            transaction.commit();
        } catch (Exception ex) {
            rollBackTran(transaction);
            throw new RegisterException("Cannot register PCMembers");
        } finally {
            closeSession(session);
        }
    }

    private void closeSession(Session session) {
        if (session != null) {
            session.close();
        }
    }
}