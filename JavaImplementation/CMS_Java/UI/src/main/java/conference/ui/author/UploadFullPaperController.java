package conference.ui.author;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

//Asta apare dar daca a fost acceptat paper-ul.
//Daca nu, apare un Message box cum ca a fost Rejected
public class UploadFullPaperController implements Initializable {

    @FXML
    private Label deadline;
    @FXML
    private TextField fullPaper;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void handleButtonUpload(ActionEvent event) {
        //
    }
}