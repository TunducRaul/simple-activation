package conference.ui.admin;

import conference.model.SteeringCommitteeMember;
import conference.service.AdminService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;
import java.util.ResourceBundle;

public class ConferenceSectionController implements Initializable {
    @FXML
    private TableView tableConference;
    @FXML
    private TableColumn idSection;
    @FXML
    private TableColumn speaker;
    @FXML
    private TableColumn chair;
    @FXML
    private TableColumn presentationFile;
    @FXML
    private TableColumn date;
    @FXML
    private TableColumn adress;
    @FXML
    private TableView listenerTabel;
    @FXML
    private TableColumn idListener;
    @FXML
    private TableColumn name;
    private AdminService service;
    private SteeringCommitteeMember admin;

    public ConferenceSectionController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private void handleButtonAdd(ActionEvent event) {
        //deschide ConferenceSectionCRUD
    }

    @FXML
    private void handleButtonUpdate(ActionEvent event) {
        //deschide ConferenceSectionCRUD
    }

    @FXML
    private void handleButtonDelete(ActionEvent event) {
        //
    }

    @FXML
    private void handleButtonAddListener(ActionEvent event) {
        //
    }

    public AdminService getService() {
        return service;
    }

    public void setService(AdminService service) {
        this.service = service;
    }

    public SteeringCommitteeMember getAdmin() {
        return admin;
    }

    public void setAdmin(SteeringCommitteeMember admin) {
        this.admin = admin;
    }
}