package conference.ui.chair;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class FailedReviewsController implements Initializable {

    @FXML
    private TableView proposalsTable;
    @FXML
    private TableColumn idProposals;
    @FXML
    private TableColumn tileProposals;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void handleButtonReasign(ActionEvent event) {
        //
    }

    @FXML
    private void handleButtonRefuse(ActionEvent event) {
        //
    }

    @FXML
    private void handleButtonAccept(ActionEvent event) {
        //
    }
}
