package conference.ui.author;

import java.net.URL;
import java.util.ResourceBundle;

import conference.model.*;
import conference.service.AuthorService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;

//Asta apare doar daca e Author simplu.
//Daca e si Reviwer apare un Message box cu: "The paper is under evaluation"
public class EvaluationController implements Initializable {
    
    @FXML
    private TextArea comments;
    @FXML 
    private TableView<ProposalReviewer> reviewerTable;
    @FXML
    private TableColumn<ProposalReviewer,Integer> id;
    @FXML
    private TableColumn<ProposalReviewer,String> name;

    private AuthorService authorService;
    private IAuthor author;
    private ConferenceEdition conferenceEdition;
    private ObservableList<ProposalReviewer> model= FXCollections.observableArrayList( );


    public void setService(AuthorService authorService,IAuthor author){
        this.authorService=authorService;
        this.author=author;
        this.conferenceEdition=authorService.getCurrentEdition();
        this.model.addAll(authorService.getAllRecommendations((DocumentSubmitter)author));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        id.setCellValueFactory(new PropertyValueFactory<ProposalReviewer,Integer>("reviewerId"));
        name.setCellValueFactory(new PropertyValueFactory<ProposalReviewer,String>("reviewerName"));
        comments.setText(reviewerTable.getSelectionModel().getSelectedItem().getRecommendations());
    }
}
