package conference.ui.reviewer;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class ReviewController implements Initializable {
    
    @FXML 
    private TextField author;
    @FXML
    private TextField title;
    @FXML
    private TextField recomandation;
    @FXML
    private TextArea paper;
    @FXML
    private ComboBox<String> qualifiers;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Notele ar trebui sa fie din Model SetQualifiers... cv de genu
        qualifiers.getItems().setAll("Strong Accept", "Accept", "Weak Accept", "Borderline Paper", "Weak Reject", "Reject", "Strong Reject");
        qualifiers.setStyle("-fx-font: 14px \"Serif\";");
    }

    @FXML
    private void handleButtonOK(ActionEvent event) {
        //
    }
}
