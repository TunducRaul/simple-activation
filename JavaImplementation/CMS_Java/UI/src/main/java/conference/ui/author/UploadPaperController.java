package conference.ui.author;

import java.net.URL;
import java.util.*;

import conference.model.*;
import conference.service.AuthorService;
import conference.utils.BUSNotifier;
import conference.utils.exceptions.UploadException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class UploadPaperController implements Initializable {

    @FXML
    private TextField titleField;
    @FXML
    private TextField topicsField;
    @FXML
    private TextField keywordsField;
    @FXML
    private TextField abstractPaperField;
    @FXML
    private TextField fullPaperField;

    @FXML
    private CheckBox uploadFullPaper;
    @FXML
    private Label proposalDeadline;

    private AuthorService authorService;
    private IAuthor author;
    private ConferenceEdition conferenceEdition;

    public Date getDeadline(DeadlineType type){
        Date date=new Date();
        Set<Deadline>deadlines=conferenceEdition.getDeadlines();
        List<Deadline> deadlineList= new ArrayList<>();
        deadlineList.addAll(deadlines);
        for(Deadline deadline:deadlineList){
            if(deadline.getType()==type){
                date=deadline.getDate();
            }
        }
        return date;
    }

    public void setService(AuthorService authorService,IAuthor author){
        this.authorService=authorService;
        this.author=author;
        this.conferenceEdition=authorService.getCurrentEdition();
        if(!conferenceEdition.getCanUpload()){
            fullPaperField.setEditable(false);
            uploadFullPaper.setDisable(true);

        }

    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    //    proposalDeadline.setText("Proposal upload can be done until: " + getDeadline(DeadlineType.CALL_FOR_PAPERS));
    }

    @FXML
    private void handleButtonUpload(ActionEvent event) {
        try{
            String title = titleField.getText();
            String topics = topicsField.getText();
            String keywords = keywordsField.getText();
            String abstractPaper = abstractPaperField.getText();
            String fullPaper = fullPaperField.getText();
            authorService.uploadPaper(title,topics,keywords,abstractPaper,fullPaper,(DocumentSubmitter)author);
            clearFields();
        } catch (UploadException e) {
            BUSNotifier.error(e.getMessage());
            e.printStackTrace();
        }catch (Exception e){
            BUSNotifier.error(e.getMessage());
            e.printStackTrace();
        }
    }


    public void clearFields(){
        titleField.setText("");
        topicsField.setText("");
        keywordsField.setText("");
        abstractPaperField.setText("");
        fullPaperField.setText("");
    }
}