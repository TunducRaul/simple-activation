package conference.ui.admin;

import conference.model.PCMember;
import conference.model.SteeringCommitteeMember;
import conference.service.AdminService;
import conference.utils.BUSNotifier;
import conference.utils.exceptions.RegisterException;
import conference.utils.exceptions.SelectionException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class CreateConferenceFirstStepController implements Initializable {
    @FXML
    private TextField affiliationField;
    @FXML
    private TextField emailAddressField;
    @FXML
    private TextField webPageField;
    @FXML
    private TableView<SteeringCommitteeMember> SCMembersTable;
    @FXML
    private TableColumn<SteeringCommitteeMember, Integer> SCMembersIdColumn;
    @FXML
    private TableColumn<SteeringCommitteeMember, String> SCMembersNameColumn;
    @FXML
    private TableView<PCMember> PCMembersTable;
    @FXML
    private TableColumn<PCMember, Integer> PCMembersIdColumn;
    @FXML
    private TableColumn<PCMember, String> PCMembersNameColumn;
    private ObservableList<SteeringCommitteeMember> SCMembersModel;
    private ObservableList<PCMember> PCMembersModel;
    private List<PCMember> PCMembersList;
    private List<SteeringCommitteeMember> SCMembersList;
    private AdminService service;
    private Stage stage;
    private SteeringCommitteeMember admin;

    public CreateConferenceFirstStepController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        SCMembersIdColumn.setCellValueFactory(new PropertyValueFactory<>("Id"));
        SCMembersNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        PCMembersIdColumn.setCellValueFactory(new PropertyValueFactory<>("Id"));
        PCMembersNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        SCMembersModel = FXCollections.observableArrayList();
        PCMembersModel = FXCollections.observableArrayList();
        SCMembersTable.setItems(SCMembersModel);
        PCMembersTable.setItems(PCMembersModel);
    }

    @FXML
    public void handleAddSCMember(ActionEvent event) {
        PCMember pcMember = PCMembersTable.getSelectionModel().getSelectedItem();
        if (pcMember == null) {
            BUSNotifier.error("No PCMember selected");
        } else {
            SteeringCommitteeMember SCMember = new SteeringCommitteeMember(pcMember.getFirstName(), pcMember.getLastName(),
                    pcMember.getId(), pcMember.getIsAdmin(), pcMember.getUsername(), pcMember.getPassword());
            SCMembersList.add(SCMember);
            PCMembersList.remove(pcMember);
            updateView();
        }
    }

    @FXML
    public void handleAddPCMember(ActionEvent event) {
        try {
            validateFields();
            SteeringCommitteeMember SCMember = SCMembersTable.getSelectionModel().getSelectedItem();
            if (SCMember == null) {
                throw new SelectionException("No SCMember selected");
            }
            String affiliation = affiliationField.getText();
            String email = emailAddressField.getText();
            String webPage = webPageField.getText();
            PCMember PCMember = new PCMember(SCMember.getFirstName(), SCMember.getLastName(), SCMember.getId(),
                    SCMember.getIsAdmin(), SCMember.getUsername(), SCMember.getPassword(), affiliation, email, webPage);
            PCMembersList.add(PCMember);
            SCMembersList.remove(SCMember);
            updateView();
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleSubmit(ActionEvent event) {
        if (PCMembersList.size() == 0) {
            BUSNotifier.error("No PCMember added to Conference");
        } else {
            try {
                initSecondStep();
            } catch (Exception ex) {
                BUSNotifier.error(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public AdminService getService() {
        return service;
    }

    public void setService(AdminService service) {
        this.service = service;
        try {
            PCMembersList = service.getAllPCMembers();
            SCMembersList = service.getAllSCMembers();
            updateView();
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public SteeringCommitteeMember getAdmin() {
        return admin;
    }

    public void setAdmin(SteeringCommitteeMember admin) {
        this.admin = admin;
    }

    private void initSecondStep() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin/create_conference_second_step.fxml"));
        Parent root = loader.load();
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        CreateConferenceSecondStepController controller = loader.getController();
        controller.setService(service);
        controller.setAdmin(admin);
        controller.setPCMembers(PCMembersList);
        controller.setStage(stage);
        stage.show();
        this.stage.close();
    }

    private void validateFields() throws RegisterException {
        String affiliation = affiliationField.getText();
        String email = emailAddressField.getText();
        String webPage = webPageField.getText();
        if (affiliation.equals("") || email.equals("") || webPage.equals("")) {
            throw new RegisterException("Detected empty fields");
        }
    }

    private void updateView() {
        affiliationField.clear();
        emailAddressField.clear();
        webPageField.clear();
        PCMembersModel.setAll(PCMembersList);
        SCMembersModel.setAll(SCMembersList);
    }
}