package conference.ui.chair;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class ConferenceEditionController implements Initializable {

    @FXML
    private TextField date;
    @FXML
    private TableView deadlineTable;
    @FXML
    private TableColumn idDeadline;
    @FXML
    private TableColumn typeDeadline;
    @FXML
    private TableColumn dateDeadline;
    @FXML
    private TableView conferenceTable;
    @FXML
    private TableColumn idConference;
    @FXML
    private TableColumn name;
    @FXML
    private TableColumn startDate;
    @FXML
    private TableColumn endDate;
    @FXML
    private TableColumn canUpload;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void handleButtonModify(ActionEvent event) {
        //
    }
}
