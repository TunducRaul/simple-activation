package conference.ui.admin;

import conference.model.ConferenceEdition;
import conference.model.Deadline;
import conference.model.DeadlineType;
import conference.model.SteeringCommitteeMember;
import conference.service.AdminService;
import conference.utils.BUSNotifier;
import conference.utils.exceptions.ProviderException;
import conference.utils.exceptions.RegisterException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ConferenceController implements Initializable {
    @FXML
    private TextField dateField;
    @FXML
    private TableView<Deadline> deadlineTable;
    @FXML
    private TableColumn idDeadlineColumn;
    @FXML
    private TableColumn typeDeadlineColumn;
    @FXML
    private TableColumn dateDeadlineColumn;
    @FXML
    private TableView<ConferenceEdition> conferenceTable;
    @FXML
    private TableColumn idConferenceColumn;
    @FXML
    private TableColumn nameConferenceColumn;
    @FXML
    private TableColumn startDate;
    @FXML
    private TableColumn endDate;
    @FXML
    private TableColumn canUpload;
    @FXML
    private ComboBox<DeadlineType> deadlineTypeCB;
    private ObservableList<ConferenceEdition> conferenceEditionModel;
    private ObservableList<Deadline> deadlineModel;
    private AdminService service;
    private SteeringCommitteeMember admin;

    public ConferenceController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        deadlineTypeCB.getItems().setAll(
                DeadlineType.BIDDING,
                DeadlineType.UPLOAD_ABSTRACT_PAPER,
                DeadlineType.UPLOAD_FULL_PAPER,
                DeadlineType.CALL_FOR_PAPERS
        );
        idDeadlineColumn.setCellValueFactory(new PropertyValueFactory<Deadline, Integer>("deadlineId"));
        typeDeadlineColumn.setCellValueFactory(new PropertyValueFactory<Deadline, DeadlineType>("type"));
        dateDeadlineColumn.setCellValueFactory(new PropertyValueFactory<Deadline, Date>("date"));
        idConferenceColumn.setCellValueFactory(new PropertyValueFactory<ConferenceEdition, Integer>("id"));
        nameConferenceColumn.setCellValueFactory(new PropertyValueFactory<ConferenceEdition, String>("name"));
        startDate.setCellValueFactory(new PropertyValueFactory<ConferenceEdition, Date>("startDate"));
        endDate.setCellValueFactory(new PropertyValueFactory<ConferenceEdition, Date>("stopDate"));
        canUpload.setCellValueFactory(new PropertyValueFactory<ConferenceController, Boolean>("canUpload"));
        deadlineModel = FXCollections.observableArrayList();
        conferenceEditionModel = FXCollections.observableArrayList();
        deadlineTable.setItems(deadlineModel);
        conferenceTable.setItems(conferenceEditionModel);
    }

    @FXML
    private void handleButtonAddDeadline(ActionEvent event) {
        try {
            DeadlineType deadlineType = deadlineTypeCB.getSelectionModel().getSelectedItem();
            if (deadlineType == null) {
                throw new RegisterException("Empty deadline");
            }
            String dateString = dateField.getText();
            Date date = parseStringToDate(dateString);
            service.addDeadline(date, deadlineType);
            clearFields();
            updateDeadlineTable();
            BUSNotifier.information("Deadline added successfully!");
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleButtonModify(ActionEvent event) {
        try {
            Deadline deadline = deadlineTable.getSelectionModel().getSelectedItem();
            DeadlineType newDeadlineType = deadlineTypeCB.getSelectionModel().getSelectedItem();
            if (newDeadlineType == null) {
                throw new RegisterException("Empty deadline");
            }
            String dateString = dateField.getText();
            Date newDate = parseStringToDate(dateString);
            service.updateDeadline(newDate, newDeadlineType, deadline);
            clearFields();
            updateDeadlineTable();
            BUSNotifier.information("Deadline was modified successfully!");
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleButtonCreate(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin/create_conference_first_step.fxml"));
            Parent root = loader.load();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            CreateConferenceFirstStepController controller = loader.getController();
            controller.setService(service);
            controller.setAdmin(admin);
            controller.setStage(stage);
            stage.show();
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleButtonDelete(ActionEvent event) {
        try {
            ConferenceEdition conferenceEdition = conferenceTable.getSelectionModel().getSelectedItem();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText(null);
            alert.setContentText("Are you sure you want to delete the conference ?");
            Optional<ButtonType> option = alert.showAndWait();
            if (option.isPresent() && option.get() == ButtonType.OK) {
                service.removeConferenceEdition(conferenceEdition);
                updateConferenceEditionTable();
            }
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleRowClicked(MouseEvent event) {
        try {
            ConferenceEdition conferenceEdition = conferenceTable.getSelectionModel().getSelectedItem();
            Set<Deadline> deadlines = conferenceEdition.getDeadlines();
            deadlineModel.setAll(deadlines);
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }


    public AdminService getService() {
        return service;
    }

    public void setService(AdminService service) {
        this.service = service;
        try {
            conferenceEditionModel.setAll(service.getAllConferenceEditions());
            deadlineModel.setAll(service.getAllDeadlines());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public SteeringCommitteeMember getAdmin() {
        return admin;
    }

    public void setAdmin(SteeringCommitteeMember admin) {
        this.admin = admin;
    }

    private void updateDeadlineTable() {
        Set<Deadline> deadlineList = service.getAllDeadlines();
        deadlineModel.setAll(deadlineList);
    }

    private void updateConferenceEditionTable() throws ProviderException {
        List<ConferenceEdition> conferenceEditionList = service.getAllConferenceEditions();
        conferenceEditionModel.setAll(conferenceEditionList);
    }

    private Date parseStringToDate(String date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.parse(date);
    }

    private void clearFields() {
        this.dateField.clear();
        this.deadlineTypeCB.getSelectionModel().select(-1);
    }
}