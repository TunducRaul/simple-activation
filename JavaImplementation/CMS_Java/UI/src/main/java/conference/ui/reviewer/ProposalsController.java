package conference.ui.reviewer;

import java.net.URL;
import java.util.ResourceBundle;

import conference.model.IAuthor;
import conference.service.ReviewerService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class ProposalsController implements Initializable {
    
    @FXML 
    private TableView reviewerTable;
    @FXML
    private TableColumn id;
    @FXML
    private TableColumn title;

    ReviewerService reviewerService;
    IAuthor author;



    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void handleButtonReview(ActionEvent event) {
        //cand dai click se deschide Review.fxml
    }
}
