package conference.ui.listener;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class ListenerController implements Initializable {

    @FXML
    private TableView table;
    @FXML
    private TableColumn idSection;
    @FXML
    private TableColumn speaker;
    @FXML
    private TableColumn chair;
    @FXML
    private TableColumn presentationFile;
    @FXML
    private TableColumn date;
    @FXML
    private TableColumn adress;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void handleButtonPay(ActionEvent event) {
        //
    }
}
