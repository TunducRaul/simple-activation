package conference.ui.author;

import conference.service.AuthorService;
import conference.utils.BUSNotifier;
import conference.utils.exceptions.RegisterException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class RegisterController implements Initializable {

    @FXML
    private TextField affiliationField;
    @FXML
    private TextField mailField;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;

    private AuthorService authorService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setService(AuthorService authorService){
        this.authorService=authorService;
    }
    @FXML
    private void handleButtonRegister(ActionEvent event) {

        try {
            String affiliation = affiliationField.getText();
            String email = mailField.getText();
            String username = usernameField.getText();
            String password = passwordField.getText();
            authorService.registerAuthor(affiliation, email, username, password);
            clearFields();
        } catch (RegisterException exception) {
            BUSNotifier.error(exception.getMessage());
        } catch (Exception e) {
            BUSNotifier.error(e.getMessage());
        }
    }


    public void clearFields(){
        affiliationField.setText("");
        mailField.setText("");
        usernameField.setText("");
        passwordField.setText("");
    }


}
