package conference.ui.co_chair;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;

public class CoChairController implements Initializable {

    @FXML
    private MenuItem conferenceEdition;
    @FXML
    private MenuItem conferenceSectionTable;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }

    @FXML
    public void handleConferenceEdition() {
        //
    }

    @FXML
    public void handleConferenceSectionTable() {
        //
    }

//    !!! EXAMPLE:
//    SarcinaCRUD = ConferenceEdition / ConferenceSectionTable
//    @FXML
//    public void handleSarcinaCRUD(){
//        mainApp.initSarcinaViewLayout();
//    }

// asta era in main:    
//    public void initSarcinaViewLayout() {
//        try {
//            // Load post view.
//            FXMLLoader loader = new FXMLLoader();
//            loader.setLocation(Main.class.getResource("/view/SarcinaView.fxml"));
//            centerLayout = (AnchorPane) loader.load();
//            rootLayout.setCenter(centerLayout);
//            //set the service and the model for controller class
//            SarcinaViewController viewCtrl = loader.getController();
//            viewCtrl.setService(sarcinaService);
//            sarcinaService.addObserver(viewCtrl);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
