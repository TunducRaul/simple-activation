package conference.ui.admin;

import conference.model.SteeringCommitteeMember;
import conference.service.AdminService;
import conference.utils.BUSNotifier;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

public class AdminController implements Initializable {
    @FXML
    private BorderPane borderPane;
    private AdminService service;
    private SteeringCommitteeMember admin;

    public AdminController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    public void handleConference() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin/conference.fxml"));
            borderPane.setCenter(loader.load());
            ConferenceController conferenceController = loader.getController();
            conferenceController.setService(service);
            conferenceController.setAdmin(admin);
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleConferenceSection() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin/conference_section.fxml"));
            borderPane.setCenter(loader.load());
            ConferenceSectionController controller = loader.getController();
            controller.setService(service);
            controller.setAdmin(admin);
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleAcceptedAuthors() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin/accepted_authors.fxml"));
            borderPane.setCenter(loader.load());
            AcceptedAuthorsController controller = loader.getController();
            controller.setService(service);
            controller.setAdmin(admin);
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public SteeringCommitteeMember getAdmin() {
        return admin;
    }

    public void setAdmin(SteeringCommitteeMember admin) {
        this.admin = admin;
    }

    public AdminService getService() {
        return service;
    }

    public void setService(AdminService service) {
        this.service = service;
    }
}