package conference.ui.assign_papers;

import conference.model.Proposal;
import conference.model.Reviewer;
import conference.model.SteeringCommitteeMember;
import conference.service.AdminService;
import conference.utils.BUSNotifier;
import conference.utils.exceptions.ProviderException;
import conference.utils.exceptions.SelectionException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

public class AssignPapersController implements Initializable {
    @FXML
    private TableView<Proposal> proposalsTabel;
    @FXML
    private TableColumn<Proposal, Integer> proposalIdColumn;
    @FXML
    private TableColumn<Proposal, String> titleColumn;
    @FXML
    private TableView<Reviewer> reviewerTable;
    @FXML
    private TableColumn<Reviewer, Integer> reviewerIdColumn;
    @FXML
    private TableColumn<Reviewer, String> reviewerNameColumn;
    private ObservableList<Proposal> proposalModel;
    private ObservableList<Reviewer> reviewerModel;
    private AdminService service;
    private SteeringCommitteeMember admin;

    public AssignPapersController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        proposalIdColumn.setCellValueFactory(new PropertyValueFactory<>("proposalID"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        reviewerIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        reviewerNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        proposalModel = FXCollections.observableArrayList();
        reviewerModel = FXCollections.observableArrayList();
        proposalsTabel.setItems(proposalModel);
        reviewerTable.setItems(reviewerModel);
    }

    @FXML
    private void handleButtonAssign(ActionEvent event) {
        try {
            Reviewer reviewer = reviewerTable.getSelectionModel().getSelectedItem();
            if (reviewer == null) {
                throw new SelectionException("No reviewer selected");
            }
            Proposal proposal = proposalsTabel.getSelectionModel().getSelectedItem();
            if (proposal == null) {
                throw new SelectionException("No proposal selected");
            }
            //service.assignPaper(proposal, reviewer);
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleRowClicked(MouseEvent event) {
        try {
            Proposal proposal = proposalsTabel.getSelectionModel().getSelectedItem();
            if (proposal == null) {
                throw new SelectionException("No paper selected!");
            }
            Set<Reviewer> reviewerSet = proposal.getReviewers();
            reviewerModel.setAll(reviewerSet);
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public SteeringCommitteeMember getAdmin() {
        return admin;
    }

    public void setAdmin(SteeringCommitteeMember admin) {
        this.admin = admin;
    }

    public AdminService getService() {
        return service;
    }

    public void setService(AdminService service) {
        this.service = service;
        try {
            this.proposalModel.setAll(service.getProposalsAfterBiddingProcess());
            this.reviewerModel.setAll(service.getAllReviewers());
        } catch (ProviderException e) {
            BUSNotifier.error(e.getMessage());
            e.printStackTrace();
        }
    }
}