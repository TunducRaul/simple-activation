package conference.ui.admin;

import conference.model.*;
import conference.service.AdminService;
import conference.utils.BUSNotifier;
import conference.utils.exceptions.RegisterException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.net.URL;
import java.util.*;

public class CreateConferenceSecondStepController implements Initializable {
    @FXML
    private TextField nameField;
    @FXML
    private TextField startDateField;
    @FXML
    private TextField endDateField;
    @FXML
    private CheckBox canUpdateCheckBox;
    @FXML
    private TableView<PCMember> pcMembersTable;
    @FXML
    private TableColumn idPC;
    @FXML
    private TableColumn namePC;
    private ObservableList<PCMember> pcMembersModel;
    private AdminService service;
    private SteeringCommitteeMember admin;
    private Stage stage;
    private List<PCMember> pcMembers;
    private List<PCMember> savePCMembersList;
    private Chair chair;
    private Set<CoChair> coChairs;
    private Set<Reviewer> reviewers;
    private Integer conferenceAdminId;

    public CreateConferenceSecondStepController() {
        this.pcMembers = new ArrayList<>();
        this.coChairs = new HashSet<>();
        this.reviewers = new HashSet<>();
        this.conferenceAdminId = -1;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idPC.setCellValueFactory(new PropertyValueFactory<SteeringCommitteeMember, Integer>("id"));
        namePC.setCellValueFactory(new PropertyValueFactory<SteeringCommitteeMember, SteeringCommitteeMember>("firstName"));
        pcMembersModel = FXCollections.observableArrayList();
        pcMembersTable.setItems(pcMembersModel);
    }

    @FXML
    private void handleButtonSetChair(ActionEvent event) throws RegisterException {
        if (chair != null) {
            BUSNotifier.error("Chair already set");
        } else {
            try {
                PCMember pcMember = pcMembersTable.getSelectionModel().getSelectedItem();
                if (this.conferenceAdminId == -1) {
                    askForConferenceAdmin(pcMember);
                }
                chair = new Chair();
                chair.setId(pcMember.getId());
                chair.setUsername(pcMember.getUsername());
                chair.setPassword(pcMember.getPassword());
                chair.setFirstName(pcMember.getFirstName());
                chair.setLastName(pcMember.getLastName());
                chair.setIsAdmin(pcMember.getIsAdmin());
                chair.setAffiliation(pcMember.getAffiliation());
                chair.setEmailAddress(pcMember.getEmailAddress());
                chair.setWebPage(pcMember.getWebPage());
                pcMembers.remove(pcMember);
                updateView();
            } catch (Exception ex) {
                BUSNotifier.error(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    @FXML
    private void handleButtonAddCoChair(ActionEvent event) {
        try {
            PCMember pcMember = pcMembersTable.getSelectionModel().getSelectedItem();
            if (this.conferenceAdminId == -1) {
                askForConferenceAdmin(pcMember);
            }
            CoChair coChair = new CoChair();
            coChair.setId(pcMember.getId());
            coChair.setUsername(pcMember.getUsername());
            coChair.setPassword(pcMember.getPassword());
            coChair.setFirstName(pcMember.getFirstName());
            coChair.setLastName(pcMember.getLastName());
            coChair.setIsAdmin(pcMember.getIsAdmin());
            coChair.setAffiliation(pcMember.getAffiliation());
            coChair.setEmailAddress(pcMember.getEmailAddress());
            coChair.setWebPage(pcMember.getWebPage());
            coChairs.add(coChair);
            pcMembers.remove(pcMember);
            updateView();
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleButtonAddReviewer(ActionEvent event) {
        try {
            PCMember pcMember = pcMembersTable.getSelectionModel().getSelectedItem();
            Reviewer reviewer = new Reviewer();
            reviewer.setId(pcMember.getId());
            reviewer.setUsername(pcMember.getUsername());
            reviewer.setPassword(pcMember.getPassword());
            reviewer.setFirstName(pcMember.getFirstName());
            reviewer.setLastName(pcMember.getLastName());
            reviewer.setIsAdmin(pcMember.getIsAdmin());
            reviewer.setAffiliation(pcMember.getAffiliation());
            reviewer.setEmailAddress(pcMember.getEmailAddress());
            reviewer.setWebPage(pcMember.getWebPage());
            reviewers.add(reviewer);
            pcMembers.remove(pcMember);
            updateView();
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleButtonSubmit(ActionEvent event) {
        try {
            validateConferenceEdition();
            String name = nameField.getText();
            String startDateString = startDateField.getText();
            String endDateString = endDateField.getText();
            Boolean canUpload = canUpdateCheckBox.isSelected();
            service.saveConferenceEdition(name, startDateString, endDateString,
                    savePCMembersList, chair, coChairs, reviewers, canUpload, conferenceAdminId);
            updateView();
            BUSNotifier.information("Conference was created successfully!");
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public AdminService getService() {
        return service;
    }

    public void setService(AdminService service) {
        this.service = service;
    }

    public SteeringCommitteeMember getAdmin() {
        return admin;
    }

    public void setAdmin(SteeringCommitteeMember admin) {
        this.admin = admin;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setPCMembers(List<PCMember> PCMembers) {
        this.pcMembers = new ArrayList<>(PCMembers);
        this.savePCMembersList = new ArrayList<>(PCMembers);
        pcMembersModel.setAll(pcMembers);
    }

    public List<PCMember> getPcMembers() {
        return pcMembers;
    }

    public void setPcMembers(List<PCMember> pcMembers) {
        this.pcMembers = pcMembers;
    }

    public List<PCMember> getSavePCMembersList() {
        return savePCMembersList;
    }

    public void setSavePCMembersList(List<PCMember> savePCMembersList) {
        this.savePCMembersList = savePCMembersList;
    }

    public Chair getChair() {
        return chair;
    }

    public void setChair(Chair chair) {
        this.chair = chair;
    }

    public Set<CoChair> getCoChairs() {
        return coChairs;
    }

    public void setCoChairs(Set<CoChair> coChairs) {
        this.coChairs = coChairs;
    }

    public Set<Reviewer> getReviewers() {
        return reviewers;
    }

    public void setReviewers(Set<Reviewer> reviewers) {
        this.reviewers = reviewers;
    }

    public Integer getConferenceAdminId() {
        return conferenceAdminId;
    }

    public void setConferenceAdminId(Integer conferenceAdminId) {
        this.conferenceAdminId = conferenceAdminId;
    }

    private void askForConferenceAdmin(PCMember pcMember) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText(null);
        alert.setContentText("Do you want to set " + pcMember.getFirstName() + " as admin ?");
        Optional<ButtonType> option = alert.showAndWait();
        if (option.isPresent() && option.get() == ButtonType.OK) {
            this.conferenceAdminId = pcMember.getId();
        }
    }

    private void updateView() {
        pcMembersModel.setAll(pcMembers);
        this.nameField.clear();
        this.startDateField.clear();
        this.endDateField.clear();
        this.canUpdateCheckBox.setSelected(false);
    }

    private void validateConferenceEdition() throws RegisterException {
        String name = nameField.getText();
        if (name.equals("")) {
            throw new RegisterException("Name cannot be null");
        }
        if (chair == null || coChairs.isEmpty() || reviewers.isEmpty()) {
            throw new RegisterException("Some entities were not added");
        }
        if (conferenceAdminId == -1) {
            throw new RegisterException("Conference admin is not set");
        }
    }
}