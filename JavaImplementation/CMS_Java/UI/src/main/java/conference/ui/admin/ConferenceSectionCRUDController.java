package conference.ui.admin;

import conference.model.SteeringCommitteeMember;
import conference.service.AdminService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class ConferenceSectionCRUDController implements Initializable {
    @FXML
    private TextField speaker;
    @FXML
    private TextField chair;
    @FXML
    private TextField prezentation;
    @FXML
    private TextField date;
    @FXML
    private TextField adress;
    private AdminService service;
    private SteeringCommitteeMember admin;

    public ConferenceSectionCRUDController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void handleButtonSubmit(ActionEvent event) {
        //
    }

    public AdminService getService() {
        return service;
    }

    public void setService(AdminService service) {
        this.service = service;
    }

    public SteeringCommitteeMember getAdmin() {
        return admin;
    }

    public void setAdmin(SteeringCommitteeMember admin) {
        this.admin = admin;
    }
}