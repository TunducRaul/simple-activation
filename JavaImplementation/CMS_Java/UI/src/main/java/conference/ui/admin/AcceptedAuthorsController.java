package conference.ui.admin;

import conference.model.DocumentSubmitter;
import conference.model.IAuthor;
import conference.model.SteeringCommitteeMember;
import conference.service.AdminService;
import conference.utils.BUSNotifier;
import conference.utils.exceptions.SelectionException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

public class AcceptedAuthorsController implements Initializable {
    @FXML
    private TableView<IAuthor> authorsTable;
    @FXML
    private TableColumn<DocumentSubmitter, Integer> idColumn;
    @FXML
    private TableColumn<DocumentSubmitter, String> nameColumn;
    private ObservableList<IAuthor> authorsModel;
    private AdminService service;
    private SteeringCommitteeMember admin;

    public AcceptedAuthorsController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        authorsModel = FXCollections.observableArrayList();
        authorsTable.setItems(authorsModel);
    }

    @FXML
    private void handleButtonSendMail(ActionEvent event) {
        try {
            IAuthor author = authorsTable.getSelectionModel().getSelectedItem();
            if (author == null) {
                throw new SelectionException("No author selected!");
            }
            authorsModel.remove(author);
            BUSNotifier.information("Message Sent!");
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public AdminService getService() {
        return service;
    }

    public void setService(AdminService service) {
        this.service = service;
        try {
            Set<IAuthor> authors = service.getCurrentEditionAuthors();
            authorsModel = FXCollections.observableArrayList();
            authorsModel.setAll(authors);
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public SteeringCommitteeMember getAdmin() {
        return admin;
    }

    public void setAdmin(SteeringCommitteeMember admin) {
        this.admin = admin;
    }
}