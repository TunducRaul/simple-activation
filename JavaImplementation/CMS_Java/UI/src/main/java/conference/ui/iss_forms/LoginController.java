package conference.ui.iss_forms;

import conference.model.*;
import conference.service.AdminService;
import conference.service.AuthorService;
import conference.service.LoginService;
import conference.ui.admin.AdminController;
import conference.ui.author.RegisterController;
import conference.ui.author.UploadPaperController;
import conference.utils.BUSNotifier;
import conference.utils.exceptions.LoginLogoutException;
import conference.utils.SpringAutowireConfig;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

public class LoginController implements Initializable {
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private PasswordField passField;
    @FXML
    private TextField userField;
    @FXML
    private ComboBox<LoginType> loginOptions;
    private LoginService loginService;
    private AuthorService authorService;
    private Stage loginStage;
    private Map<SteeringCommitteeMember, Stage> activeStages;

    public LoginController() {
        this.activeStages = new ConcurrentHashMap<>();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginOptions.getItems().setAll(LoginType.ADMIN, LoginType.AUTHOR, LoginType.CHAIR, LoginType.CO_CHAIR, LoginType.LISTENER, LoginType.REVIEWER, LoginType.SPEAKER, LoginType.PC_MEMBER);
        loginOptions.setStyle("-fx-font: 14px \"Serif\";");
    }

    @FXML
    private void handleButtonLogin(ActionEvent event) {
        try {
            String username = userField.getText();
            String password = passField.getText();
            LoginType loginOption = loginOptions.getSelectionModel().getSelectedItem();
            switch (loginOption) {
                case ADMIN:
                    handleAdminLogin(username, password);
                    clearFields();
                    break;

                case AUTHOR:
                    handleAuthorLogin(username, password);
                    clearFields();
                    break;

                case SPEAKER:
                    handleSpeakerLogin(username, password);
                    clearFields();
                    break;

                case CHAIR:
                    handleChairLogin(username, password);
                    clearFields();
                    break;

                case CO_CHAIR:
                    handleCoChairLogin(username, password);
                    clearFields();
                    break;

                case LISTENER:
                    handleListenerLogin(username, password);
                    clearFields();
                    break;

                case REVIEWER:
                    handleReviewerLogin(username, password);
                    clearFields();
                    break;

                case PC_MEMBER:
                    handlePCMemberLogin(username, password);
                    clearFields();
                    break;

                default:
                    clearFields();
                    BUSNotifier.error("Wrong option!");
                    break;
            }
        } catch (LoginLogoutException ex) {
            BUSNotifier.error(ex.getMessage());
            clearFields();
        } catch (Exception ex) {
            BUSNotifier.error(ex.getMessage());
            clearFields();
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleButtonRegister(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/author/Register.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        Stage stage = new Stage();
        stage.setScene(scene);
        RegisterController controller = loader.getController();
        controller.setService(authorService);
        loginStage.hide();
        stage.show();
        stage.setOnCloseRequest(event1 -> loginStage.show());
    }


    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public AuthorService getAuthorService() {
        return authorService;
    }

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    public LoginService getLoginService() {
        return loginService;
    }

    public void setLoginStage(Stage loginStage) {
        this.loginStage = loginStage;
    }

    public Stage getLoginStage() {
        return loginStage;
    }

    private void handleAdminLogin(String username, String password) throws LoginLogoutException, IOException {
        SteeringCommitteeMember admin = loginService.loginAdmin(username, password);
        checkIfLoggedIn(admin);
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringAutowireConfig.class);
        AdminService adminService = context.getBean(AdminService.class);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin/admin.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        activeStages.put(admin, stage);
        stage.setScene(scene);
        AdminController controller = loader.getController();
        controller.setAdmin(admin);
        controller.setService(adminService);
        loginStage.hide();
        stage.show();
        stage.setOnCloseRequest(event -> {
            activeStages.remove(admin);
            loginStage.show();
        });
        BUSNotifier.information("Welcome Admin " + admin.getFirstName());
    }

    private void handlePCMemberLogin(String username, String password) throws LoginLogoutException {
        PCMember pcMember = loginService.loginPCMember(username, password);
        BUSNotifier.information("Welcome PCMember " + pcMember.getFirstName());
    }

    private void handleReviewerLogin(String username, String password) throws LoginLogoutException {
        Reviewer reviewer = loginService.loginReviewer(username, password);
        BUSNotifier.information("Welcome reviewer " + reviewer.getFirstName());
    }

    private void handleListenerLogin(String username, String password) throws LoginLogoutException {
        IListener listener = loginService.loginListener(username, password);
        BUSNotifier.information("Welcome listener");
    }

    private void handleAuthorLogin(String username, String password) throws LoginLogoutException, IOException {
        IAuthor author = loginService.loginAuthor(username, password);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/author/UploadPaper.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        Stage stage = new Stage();
        activeStages.put((SteeringCommitteeMember) author, stage);
        stage.setScene(scene);
        UploadPaperController controller = loader.getController();
        controller.setService(authorService, author);
        loginStage.hide();
        stage.show();
        stage.setOnCloseRequest(event1 -> {
            activeStages.remove(author);
            loginStage.show();
        });
    }

    private void handleSpeakerLogin(String username, String password) throws LoginLogoutException {
        ISpeaker speaker = loginService.loginSpeaker(username, password);
        BUSNotifier.information("Welcome Speaker");
    }

    private void handleChairLogin(String username, String password) throws LoginLogoutException {
        Chair chair = loginService.loginChair(username, password);
        if (loginService.isChairOrCoChairAdmin(chair.getId())) {
            BUSNotifier.information("Welcome admin Chair " + chair.getFirstName());
        } else {
            BUSNotifier.information("Welcome Chair " + chair.getFirstName());
        }
    }

    private void handleCoChairLogin(String username, String password) throws LoginLogoutException {
        CoChair coChair = loginService.loginCoChair(username, password);
        if (loginService.isChairOrCoChairAdmin(coChair.getId())) {
            BUSNotifier.information("Welcome admin CoChair " + coChair.getFirstName());
        } else {
            BUSNotifier.information("Welcome CoChair " + coChair.getFirstName());
        }
    }

    private void clearFields() {
        this.userField.clear();
        this.passField.clear();
        this.loginOptions.getSelectionModel().select(-1);
    }

    public void closeAllStages() {
        activeStages.forEach((steeringCommitteeMember, stageElem) -> stageElem.close());
    }

    private void checkIfLoggedIn(SteeringCommitteeMember steeringCommitteeMember) throws LoginLogoutException {
        Boolean found = false;
        for (Map.Entry<SteeringCommitteeMember, Stage> entry : activeStages.entrySet()) {
            if (entry.getKey().equals(steeringCommitteeMember)) {
                found = true;
            }
        }
        if (found) {
            throw new LoginLogoutException("Entity already logged in");
        }
    }
}