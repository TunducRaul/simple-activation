package conference.ui.payment;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class PaymentController implements Initializable {

    @FXML
    private TableView table;
    @FXML
    private TableColumn idSection;
    @FXML
    private TableColumn speaker;
    @FXML
    private TableColumn chair;
    @FXML
    private TableColumn presentationFile;
    @FXML
    private TableColumn date;
    @FXML
    private TableColumn adress;
    @FXML
    private ComboBox<String> type;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        type.getItems().setAll("Speaker", "Sesion Chair", "Listener");
        type.setStyle("-fx-font: 14px \"Serif\";");
    }

    @FXML
    private void handleButtonPay(ActionEvent event) {
        //
    }
}
