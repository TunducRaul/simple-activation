package conference.model;

import org.junit.Test;

public class ReviewerTest {
    @Test
    public void testReviewer(){
        Reviewer reviewer1 = new Reviewer("firstName", "lastName", 100, true, "user", "pass", "aff", "email", "page");
        Reviewer reviewer2 = new Reviewer("firstName", "lastName", 100, true, "user", "pass", "aff", "email", "page");

        assert reviewer1.equals(reviewer2);
    }
}
