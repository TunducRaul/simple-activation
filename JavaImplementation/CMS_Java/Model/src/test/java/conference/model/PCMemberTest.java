package conference.model;

import org.junit.Test;

import java.util.Objects;

public class PCMemberTest {
    @Test
    public void testPCMember(){
        PCMember pcMember1 = new PCMember();
        PCMember pcMember2 = new PCMember("First", "Last", 100, true, "user",
                "pass", "", "", "");
        PCMember pcMember3 = new PCMember("First", "Last", 100, true, "user",
                "pass", "affiliation", "email@address.com", "www.google.com");

        assert Objects.equals(pcMember1.getFirstName(), "");
        assert Objects.equals(pcMember1.getLastName(), "");
        assert pcMember1.getId() == -1;
        assert !pcMember1.getIsAdmin();
        assert Objects.equals(pcMember1.getUsername(), "");
        assert Objects.equals(pcMember1.getPassword(), "");
        assert Objects.equals(pcMember1.getAffiliation(), "");
        assert Objects.equals(pcMember1.getEmailAddress(), "");
        assert Objects.equals(pcMember1.getWebPage(), "");

        assert Objects.equals(pcMember2.getFirstName(), "First");
        assert Objects.equals(pcMember2.getLastName(), "Last");
        assert pcMember2.getId() == 100;
        assert pcMember2.getIsAdmin();
        assert Objects.equals(pcMember2.getUsername(), "user");
        assert Objects.equals(pcMember2.getPassword(), "pass");
        assert Objects.equals(pcMember2.getAffiliation(), "");
        assert Objects.equals(pcMember2.getEmailAddress(), "");
        assert Objects.equals(pcMember2.getWebPage(), "");

        assert Objects.equals(pcMember3.getAffiliation(), "affiliation");
        assert Objects.equals(pcMember3.getEmailAddress(), "email@address.com");
        assert Objects.equals(pcMember3.getWebPage(), "www.google.com");

        pcMember1.setFirstName("First");
        pcMember1.setLastName("Last");
        pcMember1.setId(100);
        pcMember1.setIsAdmin(true);
        pcMember1.setUsername("user");
        pcMember1.setPassword("pass");

        assert pcMember1.equals(pcMember2);

        pcMember1.setAffiliation("affiliation");
        pcMember1.setEmailAddress("email@address.com");
        pcMember1.setWebPage("www.google.com");

        assert pcMember1.equals(pcMember3);
    }
}
