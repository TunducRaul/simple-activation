package conference.model.proposal;

import conference.model.*;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class ProposalTest {
    @Test
    public void testProposal(){
        Set<Keyword> setKeyword = new HashSet<>(0);
        Set<Topic> setTopic = new HashSet<>(0);
        Set<ProposalReviewer> setProposalReviewer = new HashSet<>(0);
        Set<ProposalPCMember> setProposalPCMember = new HashSet<>(0);
        Proposal proposal1 = new Proposal();
//        Proposal proposal2 = new Proposal(100, "title", "location1", "location2", setKeyword, setTopic, true, null, setProposalReviewer, setProposalPCMember, null);

        assert proposal1.getProposalID() == -1;
        assert proposal1.getTitle() == "";
        assert proposal1.getAbstractPaperLocation() == "";
        assert proposal1.getFullPaperLocation() == "";
        assert proposal1.getIsAccepted() == false;
        assert proposal1.getOwner() == null;


//        assert proposal2.getProposalID() == 100;
//        assert proposal2.getTitle() == "title";
//        assert proposal2.getAbstractPaperLocation() == "location1";
//        assert proposal2.getFullPaperLocation() == "location2";
//        assert proposal2.getKeyword().equals(setKeyword);
//        assert proposal2.getTopic().equals(setTopic);
//        assert proposal2.getIsAccepted() == true;
//        assert proposal2.getOwner() == null;
//        assert proposal2.getProposalReviewer().equals(setProposalReviewer);
//        assert proposal2.getProposalPCMember().equals(setProposalPCMember);

        proposal1.setProposalID(100);
        proposal1.setTitle("title");
        proposal1.setAbstractPaperLocation("location1");
        proposal1.setFullPaperLocation("location2");
        proposal1.setKeyword(setKeyword);
        proposal1.setTopic(setTopic);
        proposal1.setIsAccepted(true);
        proposal1.setOwner(null);
        proposal1.setProposalReviewer(setProposalReviewer);
        proposal1.setProposalPCMember(setProposalPCMember);

//        assert proposal1.equals(proposal2);
    }
}
