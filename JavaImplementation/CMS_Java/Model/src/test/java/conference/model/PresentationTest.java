package conference.model;

import org.junit.Test;

public class PresentationTest {
    @Test
    public void testPresentation(){
        Presentation presentation1 = new Presentation();
        Presentation presentation2 = new Presentation(100, null, "loc",
                null);

        assert presentation1.getPresentationId() == -1;
        assert presentation1.getOwner() == null;
        assert presentation1.getLocation() == "";

        assert presentation2.getPresentationId() == 100;
        assert presentation2.getOwner() == null;
        assert presentation2.getLocation() == "loc";
    }
}
