package conference.model;

import org.junit.Test;

public class ProposalPCMemberTest {
    @Test
    public void testProposalPCMember(){
        ProposalPCMemberMapping mapping = new ProposalPCMemberMapping();

        ProposalPCMember proposalPCMember1 = new ProposalPCMember();
        ProposalPCMember proposalPCMember2 = new ProposalPCMember(mapping, PCMemberQualifier.COULD_EVALUATE);
        ProposalPCMember proposalPCMember3 = new ProposalPCMember(mapping, PCMemberQualifier.PLEASED);
        ProposalPCMember proposalPCMember4 = new ProposalPCMember(mapping, PCMemberQualifier.REFUSE);

        assert proposalPCMember1.getId() == null;
        assert proposalPCMember1.getQualifier() == null;
        assert proposalPCMember2.getId().equals(mapping);
        assert proposalPCMember2.getQualifier() == PCMemberQualifier.COULD_EVALUATE;
        assert proposalPCMember3.getQualifier() == PCMemberQualifier.PLEASED;
        assert proposalPCMember4.getQualifier() == PCMemberQualifier.REFUSE;

        proposalPCMember1.setId(mapping);
        proposalPCMember1.setQualifier(PCMemberQualifier.COULD_EVALUATE);

        assert proposalPCMember1.equals(proposalPCMember2);
    }
}
