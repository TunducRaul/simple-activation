package conference.model;

import org.junit.Test;

public class ProposalReviewerMappingTest {
    @Test
    public void testProposalReviewer(){
        ProposalReviewerMapping mapping1 = new ProposalReviewerMapping();
        ProposalReviewerMapping mapping2 = new ProposalReviewerMapping(new Reviewer(), new Proposal());

        assert mapping1.getReviewer() == null;
        assert mapping1.getProposal() == null;

        assert mapping2.getReviewer().equals(new Reviewer());
        assert mapping2.getProposal().equals(new Proposal());

        mapping1.setReviewer(new Reviewer());
        mapping1.setProposal(new Proposal());

        assert mapping1.equals(mapping2);
    }
}
