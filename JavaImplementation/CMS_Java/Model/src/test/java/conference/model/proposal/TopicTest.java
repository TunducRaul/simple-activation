package conference.model.proposal;

import conference.model.Topic;
import org.junit.Test;

public class TopicTest {

    @Test
    public void testTopic(){
        Topic topic1 = new Topic();
        Topic topic2 = new Topic(100, "message", null);

        assert topic1.getTopicId() == -1;
        assert topic1.getMessage() == "";

        assert topic2.getTopicId() == 100;
        assert topic2.getMessage() == "message";

        topic1.setMessage("message");
        topic1.setTopicId(100);

        assert topic1.equals(topic2);
    }
}
