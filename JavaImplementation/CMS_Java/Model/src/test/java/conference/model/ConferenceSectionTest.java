package conference.model;

import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ConferenceSectionTest {
    @Test
    public void testConferenceSection() {
        ConferenceSection conferenceSection1 = new ConferenceSection();
        ConferenceSection conferenceSection2 = new ConferenceSection(100, new Date(), "address",
                null, null, new Presentation(), -1D, -1D, null);

        assert conferenceSection1.getSectionId() == -1;
        assert conferenceSection1.getDate() == null;
        assert conferenceSection1.getAddress() == "";
        assert conferenceSection1.getSessionChair() == null;
        assert conferenceSection1.getListeners().equals(new HashSet<>());
        assert conferenceSection1.getPresentation() == null;

        assert conferenceSection2.getSectionId() == 100;
//        assert conferenceSection2.getDate() == new Date();
        assert conferenceSection2.getAddress() == "address";
        assert conferenceSection2.getSessionChair() == null;
        assert conferenceSection2.getListeners() == null;
    }
}
