package conference.model;

import org.junit.Test;

public class SteeringComitteeMemberTest {
    @Test
    public void testSteeringComitteeMember(){
        SteeringCommitteeMember scm1 = new SteeringCommitteeMember();
        SteeringCommitteeMember scm2 = new SteeringCommitteeMember("First", "Last", 169, true, "user", "pass");

        assert scm1.getFirstName() == "";
        assert scm1.getLastName() == "";
        assert scm1.getId() == -1;
        assert scm1.getIsAdmin() == false;
        assert scm1.getUsername() == "";
        assert scm1.getPassword() == "";

        assert scm2.getFirstName() == "First";
        assert scm2.getLastName() == "Last";
        assert scm2.getId() == 169;
        assert scm2.getIsAdmin() == true;
        assert scm2.getUsername() == "user";
        assert scm2.getPassword() == "pass";

        scm1.setFirstName("First");
        scm1.setLastName("Last");
        scm1.setId(169);
        scm1.setIsAdmin(true);
        scm1.setUsername("user");
        scm1.setPassword("pass");

        assert scm1.getFirstName() == "First";
        assert scm1.getLastName() == "Last";
        assert scm1.getId() == 169;
        assert scm1.getIsAdmin() == true;
        assert scm1.getUsername() == "user";
        assert scm1.getPassword() == "pass";

        assert scm1.equals(scm2);
    }

}
