package conference.model;

import org.junit.Test;

import java.util.Objects;

public class DocumentSubmitterTest {
    @Test
    public void testDocumentSubmitter() {
        DocumentSubmitter documentSubmitter1 = new DocumentSubmitter();
        DocumentSubmitter documentSubmitter2 = new DocumentSubmitter("First", "Last",
                100, true, "user", "pass", true, "affiliation",
                "email@address.com", null, null);

        assert !documentSubmitter1.getIsReviewer();
        assert Objects.equals(documentSubmitter1.getAffiliation(), "");
        assert Objects.equals(documentSubmitter1.getEmailAddress(), "");
        assert !documentSubmitter1.getIsReviewer();

        assert documentSubmitter2.getIsReviewer();
        assert Objects.equals(documentSubmitter2.getAffiliation(), "affiliation");
        assert Objects.equals(documentSubmitter2.getEmailAddress(), "email@address.com");
        assert documentSubmitter2.getIsReviewer();

        documentSubmitter1.setIsReviewer(true);
        documentSubmitter1.setAffiliation("affiliation");
        documentSubmitter1.setEmailAddress("email@address.com");

        assert documentSubmitter1.getIsReviewer();
        assert Objects.equals(documentSubmitter1.getAffiliation(), "affiliation");
        assert Objects.equals(documentSubmitter1.getEmailAddress(), "email@address.com");
        assert documentSubmitter1.getIsReviewer();
    }
}