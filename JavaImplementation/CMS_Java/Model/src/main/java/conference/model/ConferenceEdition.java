package conference.model;

import java.util.*;

public class ConferenceEdition {
    private Integer id;
    private String name;
    private Date startDate;
    private Date stopDate;
    private Boolean canUpload = false;
    private Chair chair;
    private Integer currentAdminId;
    private Set<Deadline> deadlines;
    private Set<ConferenceSection> conferenceSections;
    private Set<Proposal> proposals;
    private Set<IAuthor> authors;
    private Set<Reviewer> reviewers;
    private Set<CoChair> coChairs;
    private Set<PCMember> pcMembers;

    public ConferenceEdition() {
        this.id = -1;
        this.name = "";
        this.startDate = null;
        this.stopDate = null;
        this.canUpload = false;
        this.currentAdminId = -1;
        this.chair = null;
        this.deadlines = new HashSet<>(0);
        this.conferenceSections = new HashSet<>(0);
        this.proposals = new HashSet<>(0);
        this.authors = new HashSet<>(0);
        this.reviewers = new HashSet<>(0);
        this.coChairs = new HashSet<>(0);
        this.pcMembers = new HashSet<>(0);
    }

    public ConferenceEdition(Integer id,
                             String name,
                             Date startDate,
                             Date stopDate,
                             Boolean canUpload,
                             Integer currentAdminId,
                             Chair chair,
                             Set<Deadline> deadlines,
                             Set<ConferenceSection> conferenceSections,
                             Set<Proposal> proposals,
                             Set<IAuthor> authors,
                             Set<Reviewer> reviewers,
                             Set<CoChair> coChairs,
                             Set<PCMember> pcMembers) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.canUpload = canUpload;
        this.currentAdminId = currentAdminId;
        this.chair = chair;
        this.deadlines = deadlines;
        this.conferenceSections = conferenceSections;
        this.proposals = proposals;
        this.authors = authors;
        this.reviewers = reviewers;
        this.coChairs = coChairs;
        this.pcMembers = pcMembers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public Boolean getCanUpload() {
        return canUpload;
    }

    public void setCanUpload(Boolean canUpload) {
        this.canUpload = canUpload;
    }

    public Chair getChair() {
        return chair;
    }

    public void setChair(Chair chair) {
        this.chair = chair;
    }

    public Set<Deadline> getDeadlines() {
        return deadlines;
    }

    public void setDeadlines(Set<Deadline> deadlines) {
        this.deadlines = deadlines;
    }

    public Set<ConferenceSection> getConferenceSections() {
        return conferenceSections;
    }

    public void setConferenceSections(Set<ConferenceSection> conferenceSections) {
        this.conferenceSections = conferenceSections;
    }

    public Set<Proposal> getProposals() {
        return proposals;
    }

    public void setProposals(Set<Proposal> proposals) {
        this.proposals = proposals;
    }

    public Set<Proposal> getFailedReviews() {
        Set<Proposal> failedReviews = new HashSet<>(0);
        proposals.forEach(proposal -> {
            if (!proposal.getIsAccepted()) {
                failedReviews.add(proposal);
            }
        });
        return failedReviews;
    }

    public Set<IAuthor> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<IAuthor> authors) {
        this.authors = authors;
    }

    public Set<Reviewer> getReviewers() {
        return reviewers;
    }

    public void setReviewers(Set<Reviewer> reviewers) {
        this.reviewers = reviewers;
    }

    public Set<CoChair> getCoChairs() {
        return coChairs;
    }

    public void setCoChairs(Set<CoChair> coChairs) {
        this.coChairs = coChairs;
    }

    public Set<PCMember> getPcMembers() {
        return pcMembers;
    }

    public void setPcMembers(Set<PCMember> pcMembers) {
        this.pcMembers = pcMembers;
    }

    public Integer getCurrentAdminId() {
        return currentAdminId;
    }

    public void setCurrentAdminId(Integer currentAdminId) {
        this.currentAdminId = currentAdminId;
    }

    public void addPcMember(PCMember pcMember) {
        this.pcMembers.add(pcMember);
    }

    public void addCoChair(CoChair coChair) {
        this.coChairs.add(coChair);
    }

    public void addReviewer(Reviewer reviewer) {
        this.reviewers.add(reviewer);
    }

    public void addAuthor(IAuthor author) {
        this.authors.add(author);
    }

    public void addProposal(Proposal proposal) {
        this.proposals.add(proposal);
    }

    public void addConferenceSection(ConferenceSection conferenceSection) {
        this.conferenceSections.add(conferenceSection);
    }

    public void addDeadLine(Deadline deadline) {
        this.deadlines.add(deadline);
    }

    public Boolean existListener(IListener listener) {
        Boolean found = false;
        Iterator<ConferenceSection> iterator = conferenceSections.iterator();
        while (iterator.hasNext() && !found) {
            ConferenceSection elem = iterator.next();
            found = elem.existListener(listener);
        }
        return found;
    }

    public Boolean existSpeaker(ISpeaker speaker) {
        Boolean found = false;
        Iterator<ConferenceSection> iterator = conferenceSections.iterator();
        while (iterator.hasNext() && !found) {
            ConferenceSection elem = iterator.next();
            Presentation presentation = elem.getPresentation();
            if (presentation != null) {
                ISpeaker owner = presentation.getOwner();
                found = speaker.equals(owner);
            }
        }
        return found;
    }

    public Boolean existCoChair(CoChair coChair) {
        Boolean found = false;
        Iterator<CoChair> iterator = coChairs.iterator();
        while (iterator.hasNext() && !found) {
            CoChair elem = iterator.next();
            if (elem.equals(coChair)) {
                found = true;
            }
        }
        return found;
    }

    public Boolean existReviewer(Reviewer reviewer) {
        Boolean found = false;
        Iterator<Reviewer> iterator = reviewers.iterator();
        while (iterator.hasNext() && !found) {
            Reviewer elem = iterator.next();
            if (reviewer.equals(elem)) {
                found = true;
            }
        }
        return found;
    }

    public Boolean existPCMember(PCMember pcMember) {
        Boolean found = false;
        Iterator<PCMember> iterator = pcMembers.iterator();
        while (iterator.hasNext() && !found) {
            PCMember elem = iterator.next();
            if (pcMember.equals(elem)) {
                found = true;
            }
        }
        return found;
    }
}