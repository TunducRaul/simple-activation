package conference.model;

import java.io.Serializable;

public class ProposalReviewerMapping implements Serializable {
    private Reviewer reviewer;
    private Proposal proposal;

    public ProposalReviewerMapping() {
        this.reviewer = null;
        this.proposal = null;
    }

    public ProposalReviewerMapping(Reviewer reviewer, Proposal proposal) {
        this.reviewer = reviewer;
        this.proposal = proposal;
    }

    public Reviewer getReviewer() {
        return reviewer;
    }

    public void setReviewer(Reviewer reviewer) {
        this.reviewer = reviewer;
    }

    public Proposal getProposal() {
        return proposal;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProposalReviewerMapping that = (ProposalReviewerMapping) o;

        if (!reviewer.equals(that.reviewer)) return false;
        return proposal.equals(that.proposal);
    }

    @Override
    public int hashCode() {
        int result = reviewer.hashCode();
        result = 31 * result + proposal.hashCode();
        return result;
    }
}