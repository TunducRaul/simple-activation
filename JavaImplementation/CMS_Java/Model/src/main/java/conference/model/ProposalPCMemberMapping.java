package conference.model;

import java.io.Serializable;

public class ProposalPCMemberMapping implements Serializable{
    private PCMember pcMember;
    private Proposal proposal;

    public ProposalPCMemberMapping() {
        this.pcMember = null;
        this.proposal = null;
    }

    public ProposalPCMemberMapping(PCMember pcMember, Proposal proposal) {
        this.pcMember = pcMember;
        this.proposal = proposal;
    }

    public PCMember getPcMember() {
        return pcMember;
    }

    public void setPcMember(PCMember pcMember) {
        this.pcMember = pcMember;
    }

    public Proposal getProposal() {
        return proposal;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProposalPCMemberMapping that = (ProposalPCMemberMapping) o;

        if (!pcMember.equals(that.pcMember)) return false;
        return proposal.equals(that.proposal);
    }

    @Override
    public int hashCode() {
        int result = pcMember.hashCode();
        result = 31 * result + proposal.hashCode();
        return result;
    }
}