package conference.model;

public class ConferenceSectionListener {
    private ConferenceSectionListenerMapping id;
    private Double payment;

    public ConferenceSectionListener() {
        this.id = null;
        this.payment = -1D;
    }

    public ConferenceSectionListener(ConferenceSectionListenerMapping id, Double payment) {
        this.id = id;
        this.payment = payment;
    }

    public IListener getListener() {
        return id.getListener();
    }

    public void setListener(IListener listener) {
        id.setListener(listener);
    }

    public ConferenceSection getConferenceSection() {
        return id.getConferenceSection();
    }

    public void setConferenceSection(ConferenceSection conferenceSection) {
        id.setConferenceSection(conferenceSection);
    }

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }

    public ConferenceSectionListenerMapping getId() {
        return id;
    }

    public void setId(ConferenceSectionListenerMapping id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConferenceSectionListener that = (ConferenceSectionListener) o;

        if (!id.equals(that.id)) return false;
        return payment.equals(that.payment);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + payment.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ConferenceSectionListener{" +
                "id=" + id +
                ", payment=" + payment +
                '}';
    }
}