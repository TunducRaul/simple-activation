package conference.model;

public class Presentation {
    private Integer presentationId;
    private ISpeaker owner;
    private String location;
    private ConferenceSection section;

    public Presentation() {
        this.owner = null;
        this.location = "";
        this.presentationId = -1;
        this.section = null;
    }

    public Presentation(Integer presentationId, ISpeaker owner, String location, ConferenceSection section) {
        this.presentationId = presentationId;
        this.owner = owner;
        this.location = location;
        this.section = section;
    }

    public Integer getPresentationId() {
        return presentationId;
    }

    public void setPresentationId(Integer presentationId) {
        this.presentationId = presentationId;
    }

    public ISpeaker getOwner() {
        return owner;
    }

    public void setOwner(ISpeaker owner) {
        this.owner = owner;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ConferenceSection getSection() {
        return section;
    }

    public void setSection(ConferenceSection section) {
        this.section = section;
    }
}