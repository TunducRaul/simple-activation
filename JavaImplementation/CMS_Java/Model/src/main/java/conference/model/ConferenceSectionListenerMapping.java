package conference.model;

import java.io.Serializable;

public class ConferenceSectionListenerMapping implements Serializable{
    private IListener listener;
    private ConferenceSection conferenceSection;

    public ConferenceSectionListenerMapping() {
        this.listener = null;
        this.conferenceSection = null;
    }

    public ConferenceSectionListenerMapping(IListener listener, ConferenceSection conferenceSection) {
        this.listener = listener;
        this.conferenceSection = conferenceSection;
    }

    public IListener getListener() {
        return listener;
    }

    public void setListener(IListener listener) {
        this.listener = listener;
    }

    public ConferenceSection getConferenceSection() {
        return conferenceSection;
    }

    public void setConferenceSection(ConferenceSection conferenceSection) {
        this.conferenceSection = conferenceSection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConferenceSectionListenerMapping that = (ConferenceSectionListenerMapping) o;

        if (!listener.equals(that.listener)) return false;
        return conferenceSection.equals(that.conferenceSection);
    }

    @Override
    public int hashCode() {
        int result = listener.hashCode();
        result = 31 * result + conferenceSection.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ConferenceSectionListenerMapping{" +
                "listener=" + listener +
                ", conferenceSection=" + conferenceSection +
                '}';
    }
}
