package conference.model;

public enum ReviewerQualifier {
    STRONG_ACCEPT, ACCEPT, WEAK_ACCEPT, BORDERLINE_PAPER, WEAK_REJECT, REJECT, STRONG_REJECT
}