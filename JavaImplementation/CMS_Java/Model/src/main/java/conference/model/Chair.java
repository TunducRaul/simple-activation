package conference.model;

public class Chair extends PCMember implements ISessionChair {

    public Chair() {
        super();
    }

    public Chair(String firstName,
                 String lastName,
                 Integer id,
                 Boolean isAdmin,
                 String username,
                 String password,
                 String affiliation,
                 String emailAddress,
                 String webPage) {
        super(firstName, lastName, id, isAdmin, username, password, affiliation, emailAddress, webPage);
    }
}