package conference.model;

import java.io.Serializable;

public class ProposalReviewer implements Serializable {
    private ProposalReviewerMapping id;
    private ReviewerQualifier qualifier;
    private String recommendations;

    public ProposalReviewer() {
        this.id = null;
        this.qualifier = null;
        this.recommendations = "";
    }

    public ProposalReviewer(ProposalReviewerMapping id, ReviewerQualifier qualifier,
                            String recommendations) {
        this.id = id;
        this.qualifier = qualifier;
        this.recommendations = recommendations;
    }

    public ProposalReviewerMapping getId() {
        return id;
    }

    public void setId(ProposalReviewerMapping id) {
        this.id = id;
    }

    public ReviewerQualifier getQualifier() {
        return qualifier;
    }

    public void setQualifier(ReviewerQualifier qualifier) {
        this.qualifier = qualifier;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProposalReviewer that = (ProposalReviewer) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
