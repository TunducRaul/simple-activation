package conference.model;

import java.util.Date;

public class Deadline {
    private Date date;
    private Integer deadlineId;
    private DeadlineType type;

    public Deadline() {
        this.deadlineId = -1;
        this.date = null;
        this.type = null;
    }

    public Deadline(Date date, Integer deadlineId, DeadlineType type) {
        this.date = date;
        this.deadlineId = deadlineId;
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getDeadlineId() {
        return deadlineId;
    }

    public void setDeadlineId(Integer deadlineId) {
        this.deadlineId = deadlineId;
    }

    public DeadlineType getType() {
        return type;
    }

    public void setType(DeadlineType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Deadline deadline = (Deadline) o;

        return deadlineId.equals(deadline.deadlineId);
    }

    @Override
    public int hashCode() {
        return deadlineId.hashCode();
    }
}