package conference.model;

public class SteeringCommitteeMember {
    //TODO: Override hashCode():int and equals(obj:Object):boolean methods for serving as key in ConcurrentMap<>
    private String firstName;
    private String lastName;
    private Integer id;
    private Boolean isAdmin = false;
    private String username;
    private String password;

    public SteeringCommitteeMember() {
        this.firstName = "";
        this.lastName = "";
        this.id = -1;
        this.username = "";
        this.password = "";
    }

    public SteeringCommitteeMember(String firstName,
                                   String lastName,
                                   Integer id,
                                   Boolean isAdmin,
                                   String username,
                                   String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.isAdmin = isAdmin;
        this.username = username;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SteeringCommitteeMember that = (SteeringCommitteeMember) o;

        if (!username.equals(that.username)) return false;
        return password.equals(that.password);
    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }
}