package conference.model;

public interface IAuthor {
    Boolean getIsReviewer();

    void setIsReviewer(Boolean isReviewer);
}