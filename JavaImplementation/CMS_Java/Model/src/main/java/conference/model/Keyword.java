package conference.model;

public class Keyword {
    private Integer keywordId;
    private String message;
    private Proposal proposal;

    public Keyword() {
        this.keywordId = -1;
        this.message = "";
        this.proposal = null;
    }

    public Keyword(Integer id, String message, Proposal proposal) {
        this.keywordId = id;
        this.message = message;
        this.proposal = proposal;
    }

    public Integer getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(Integer keywordId) {
        this.keywordId = keywordId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Proposal getProposal() {
        return proposal;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Keyword keyword = (Keyword) o;

        return keywordId.equals(keyword.keywordId);
    }

    @Override
    public int hashCode() {
        return keywordId.hashCode();
    }
}