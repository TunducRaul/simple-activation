package conference.model;

public enum LoginType {
    ADMIN, CHAIR, CO_CHAIR, REVIEWER, AUTHOR, LISTENER, SPEAKER, PC_MEMBER
}
