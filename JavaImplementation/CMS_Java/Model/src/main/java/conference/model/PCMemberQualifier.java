package conference.model;

public enum PCMemberQualifier {
    PLEASED, COULD_EVALUATE, REFUSE
}