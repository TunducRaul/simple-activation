package conference.model;

public class PCMember extends SteeringCommitteeMember implements IListener {
    //TODO: Override hashCode():int and equals(obj:Object):boolean methods for serving as key in ConcurrentMap<>
    private String affiliation;
    private String emailAddress;
    private String webPage;

    public PCMember() {
        super();
        this.affiliation = "";
        this.emailAddress = "";
        this.webPage = "";
    }

    public PCMember(String firstName,
                    String lastName,
                    Integer id,
                    Boolean isAdmin,
                    String username,
                    String password,
                    String affiliation,
                    String emailAddress,
                    String webPage) {
        super(firstName, lastName, id, isAdmin, username, password);
        this.affiliation = affiliation;
        this.emailAddress = emailAddress;
        this.webPage = webPage;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getWebPage() {
        return webPage;
    }

    public void setWebPage(String webPage) {
        this.webPage = webPage;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}