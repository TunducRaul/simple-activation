package conference.model;

public class Topic {
    private Integer topicId;
    private String message;
    private Proposal proposal;

    public Topic() {
        this.topicId = -1;
        this.message = "";
        this.proposal = null;
    }

    public Topic(Integer topicId, String message, Proposal proposal) {
        this.topicId = topicId;
        this.message = message;
        this.proposal = proposal;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Proposal getProposal() {
        return proposal;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Topic topic = (Topic) o;

        return topicId.equals(topic.topicId);
    }

    @Override
    public int hashCode() {
        return topicId.hashCode();
    }
}