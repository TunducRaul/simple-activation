package conference.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ConferenceSection {
    private Integer sectionId;
    private Date date;
    private String address;
    private ISessionChair sessionChair;
    private Set<ConferenceSectionListener> listeners;
    private Presentation presentation;
    private Double sessionChairPayment;
    private Double speakerPayment;
    private ConferenceEdition conferenceEdition;

    public ConferenceSection() {
        this.sectionId = -1;
        this.date = null;
        this.address = "";
        this.sessionChair = null;
        this.presentation = null;
        this.listeners = new HashSet<>(0);
        this.sessionChairPayment = -1D;
        this.speakerPayment = -1D;
        this.conferenceEdition = null;
    }

    public ConferenceSection(Integer sectionId,
                             Date date,
                             String address,
                             ISessionChair sessionChair,
                             Set<ConferenceSectionListener> listeners,
                             Presentation presentation,
                             Double sessionChairPayment,
                             Double speakerPayment,
                             ConferenceEdition conferenceEdition) {
        this.sectionId = sectionId;
        this.date = date;
        this.address = address;
        this.sessionChair = sessionChair;
        this.listeners = listeners;
        this.presentation = presentation;
        this.sessionChairPayment = sessionChairPayment;
        this.speakerPayment = speakerPayment;
        this.conferenceEdition = conferenceEdition;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sesstionId) {
        this.sectionId = sesstionId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ISessionChair getSessionChair() {
        return sessionChair;
    }

    public void setSessionChair(ISessionChair sessionChair) {
        this.sessionChair = sessionChair;
    }

    public Set<ConferenceSectionListener> getListeners() {
        return listeners;
    }

    public Double getSessionChairPayment() {
        return sessionChairPayment;
    }

    public void setSessionChairPayment(Double sessionChairPayment) {
        this.sessionChairPayment = sessionChairPayment;
    }

    public Double getSpeakerPayment() {
        return speakerPayment;
    }

    public void setSpeakerPayment(Double speakerPayment) {
        this.speakerPayment = speakerPayment;
    }

    public void setListeners(Set<ConferenceSectionListener> listeners) {
        this.listeners = listeners;
    }

    public void addListener(ConferenceSectionListener listener) {
        this.listeners.add(listener);
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public void setPresentation(Presentation presentation) {
        this.presentation = presentation;
    }

    public ISpeaker getSpeaker() {
        return presentation.getOwner();
    }

    public ConferenceEdition getConferenceEdition() {
        return conferenceEdition;
    }

    public void setConferenceEdition(ConferenceEdition conferenceEdition) {
        this.conferenceEdition = conferenceEdition;
    }

    public Boolean existListener(IListener listener) {
        Boolean found = false;
        Iterator<ConferenceSectionListener> iterator = listeners.iterator();
        while (iterator.hasNext() && !found) {
            ConferenceSectionListener elem = iterator.next();
            if (elem.equals(elem.getListener())) {
                found = true;
            }
        }
        return found;
    }
}