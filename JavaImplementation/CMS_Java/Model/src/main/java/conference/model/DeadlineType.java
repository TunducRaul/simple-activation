package conference.model;

public enum DeadlineType {
    CALL_FOR_PAPERS, BIDDING, UPLOAD_ABSTRACT_PAPER, UPLOAD_FULL_PAPER
}