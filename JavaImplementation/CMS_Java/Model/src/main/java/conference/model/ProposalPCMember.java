package conference.model;

import java.io.Serializable;

public class ProposalPCMember implements Serializable {
    private ProposalPCMemberMapping id;
    private PCMemberQualifier qualifier;

    public ProposalPCMember() {
        this.id = null;
        this.qualifier = null;
    }

    public ProposalPCMember(ProposalPCMemberMapping id, PCMemberQualifier qualifier) {
        this.id = id;
        this.qualifier = qualifier;
    }

    public ProposalPCMemberMapping getId() {
        return id;
    }

    public void setId(ProposalPCMemberMapping id) {
        this.id = id;
    }

    public PCMemberQualifier getQualifier() {
        return qualifier;
    }

    public void setQualifier(PCMemberQualifier qualifier) {
        this.qualifier = qualifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProposalPCMember that = (ProposalPCMember) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}