package conference.model;

public class Reviewer extends PCMember {
    //TODO: Override hashCode():int and equals(obj:Object):boolean methods for serving as key in ConcurrentMap<>

    public Reviewer() {
        super();
    }

    public Reviewer(String firstName,
                    String lastName,
                    Integer id,
                    Boolean isAdmin,
                    String username,
                    String password,
                    String affiliation,
                    String emailAddress,
                    String webPage) {
        super(firstName, lastName, id, isAdmin, username, password, affiliation, emailAddress, webPage);
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}