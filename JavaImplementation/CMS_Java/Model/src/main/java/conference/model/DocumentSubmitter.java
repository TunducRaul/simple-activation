package conference.model;

public class DocumentSubmitter extends SteeringCommitteeMember implements IAuthor, ISpeaker {
    private Boolean isReviewer;
    private String affiliation;
    private String emailAddress;
    private Presentation presentation;
    private Proposal proposal;

    public DocumentSubmitter() {
        super();
        this.isReviewer = false;
        this.affiliation = "";
        this.emailAddress = "";
        this.presentation = null;
        this.proposal = null;
    }

    public DocumentSubmitter(String firstName,
                             String lastName,
                             Integer id,
                             Boolean isAdmin,
                             String username,
                             String password) {
        super(firstName, lastName, id, isAdmin, username, password);
        this.isReviewer = false;
        this.affiliation = "";
        this.emailAddress = "";
        this.presentation = null;
        this.proposal = null;
    }

    public DocumentSubmitter(String firstName,
                             String lastName,
                             Integer id,
                             Boolean isAdmin,
                             String username,
                             String password,
                             Boolean isReviewer,
                             String affiliation,
                             String emailAddress,
                             Presentation presentation,
                             Proposal proposal) {
        super(firstName, lastName, id, isAdmin, username, password);
        this.isReviewer = isReviewer;
        this.affiliation = affiliation;
        this.emailAddress = emailAddress;
        this.presentation = presentation;
        this.proposal = proposal;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public void setIsReviewer(Boolean isReviewer) {
        this.isReviewer = isReviewer;
    }

    @Override
    public Boolean getIsReviewer() {
        return isReviewer;
    }

    public Presentation getPresentation() {

        return presentation;
    }

    public void setPresentation(Presentation presentation) {
        this.presentation = presentation;
    }

    public Proposal getProposal() {
        return proposal;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }
}