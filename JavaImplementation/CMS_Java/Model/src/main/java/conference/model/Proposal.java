package conference.model;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class Proposal {
    private Integer proposalID;
    private String title;
    private String abstractPaperLocation;
    private String fullPaperLocation;
    private Set<Keyword> keyword;
    private Set<Topic> topic;
    private Set<PossibleAuthor> possibleAuthors;
    private Boolean isAccepted;
    private IAuthor owner;
    private Set<ProposalReviewer> proposalReviewer;
    private Set<ProposalPCMember> proposalPCMember;
    private Logger logger = Logger.getLogger(Proposal.class.getName());
    private ConferenceEdition conferenceEdition;

    public Proposal() {
        this.proposalID = -1;
        this.isAccepted = false;
        this.title = "";
        this.abstractPaperLocation = "";
        this.fullPaperLocation = "";
        this.keyword = new HashSet<>(0);
        this.topic = new HashSet<>(0);
        this.proposalPCMember = new HashSet<>(0);
        this.proposalReviewer = new HashSet<>(0);
        this.possibleAuthors = new HashSet<>(0);
        this.owner = null;
        this.conferenceEdition = null;
    }

    public Proposal(Integer proposalID,
                    String title,
                    String abstractPaperLocation,
                    String fullPaperLocation,
                    Set<Keyword> keyword,
                    Set<Topic> topic,
                    Set<PossibleAuthor> possibleAuthors,
                    Boolean isAccepted,
                    IAuthor owner,
                    Set<ProposalReviewer> proposalReviewer,
                    Set<ProposalPCMember> proposalPCMember,
                    ConferenceEdition conferenceEdition) {
        this.proposalID = proposalID;
        this.title = title;
        this.abstractPaperLocation = abstractPaperLocation;
        this.fullPaperLocation = fullPaperLocation;
        this.keyword = keyword;
        this.topic = topic;
        this.possibleAuthors = possibleAuthors;
        this.isAccepted = isAccepted;
        this.owner = owner;
        this.proposalReviewer = proposalReviewer;
        this.proposalPCMember = proposalPCMember;
        this.conferenceEdition = conferenceEdition;
    }

    public Integer getProposalID() {
        return proposalID;
    }

    public void setProposalID(Integer proposalID) {
        this.proposalID = proposalID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbstractPaperLocation() {
        return abstractPaperLocation;
    }

    public void setAbstractPaperLocation(String abstractPaperLocation) {
        this.abstractPaperLocation = abstractPaperLocation;
    }

    public String getFullPaperLocation() {
        return fullPaperLocation;
    }

    public void setFullPaperLocation(String fullPaperLocation) {
        this.fullPaperLocation = fullPaperLocation;
    }

    public void addKeyword(Keyword keyword) {
        this.keyword.add(keyword);
    }

    public Set<Keyword> getKeyword() {
        return keyword;
    }

    public void setKeyword(Set<Keyword> keyword) {
        this.keyword = keyword;
    }

    public Set<Topic> getTopic() {
        return topic;
    }

    public void setTopic(Set<Topic> topics) {
        this.topic = topics;
    }

    public void addTopic(Topic topic) {
        this.topic.add(topic);
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean accepted) {
        isAccepted = accepted;
    }

    public IAuthor getOwner() {
        return owner;
    }

    public void setOwner(IAuthor owner) {
        this.owner = owner;
    }

    public Set<ProposalReviewer> getProposalReviewer() {
        return proposalReviewer;
    }

    public void setProposalReviewer(Set<ProposalReviewer> proposalReviewer) {
        this.proposalReviewer = proposalReviewer;
    }

    public Set<ProposalPCMember> getProposalPCMember() {
        return proposalPCMember;
    }

    public void setProposalPCMember(Set<ProposalPCMember> proposalPCMember) {
        this.proposalPCMember = proposalPCMember;
    }

    public ConferenceEdition getConferenceEdition() {
        return conferenceEdition;
    }

    public void setConferenceEdition(ConferenceEdition conferenceEdition) {
        this.conferenceEdition = conferenceEdition;
    }

    public Set<PossibleAuthor> getPossibleAuthors() {
        return possibleAuthors;
    }

    public void setPossibleAuthors(Set<PossibleAuthor> possibleAuthors) {
        this.possibleAuthors = possibleAuthors;
    }

    public void addProposalReviewer(ProposalReviewer proposalReviewer) {
        this.proposalReviewer.add(proposalReviewer);
    }

    public void addProposalPCMember(ProposalPCMember proposalPCMember) {
        this.proposalPCMember.add(proposalPCMember);
    }

    private Boolean isAcceptedByPCMember(ProposalPCMember proposal) {
        return proposal.getQualifier() == PCMemberQualifier.PLEASED || proposal.getQualifier() == PCMemberQualifier.COULD_EVALUATE;
    }

    private Boolean isRefusedByPCMember(ProposalPCMember proposal) {
        return proposal.getQualifier() == PCMemberQualifier.REFUSE;
    }

    public Boolean isRefusedInBiddingPhase() {
        logger.info("[Proposal] verify if current proposal - " + title + " - is refused by PCMembers");
        final AtomicInteger positive = new AtomicInteger(0);
        final AtomicInteger negative = new AtomicInteger(0);
        proposalPCMember.forEach(proposal -> {
            if (isAcceptedByPCMember(proposal)) {
                positive.incrementAndGet();
            }
            if (isRefusedByPCMember(proposal)) {
                negative.incrementAndGet();
            }
        });
        return positive.get() < negative.get();
    }

    private Boolean isAcceptedByReviewer(ProposalReviewer proposal) {
        return proposal.getQualifier() == ReviewerQualifier.ACCEPT || proposal.getQualifier() == ReviewerQualifier.STRONG_ACCEPT ||
                proposal.getQualifier() == ReviewerQualifier.WEAK_ACCEPT;
    }

    private Boolean isRefusedByReviewer(ProposalReviewer proposal) {
        return proposal.getQualifier() == ReviewerQualifier.REJECT || proposal.getQualifier() == ReviewerQualifier.STRONG_REJECT ||
                proposal.getQualifier() == ReviewerQualifier.WEAK_REJECT;
    }

    public Boolean hasContradictoryQualifiers() {
        logger.info("[Proposal] verify if current proposal - " + title + " - has contradictory qualifiers.");
        final AtomicInteger positive = new AtomicInteger(0);
        final AtomicInteger negative = new AtomicInteger(0);
        proposalReviewer.forEach((proposal) -> {
            if (isAcceptedByReviewer(proposal)) {
                positive.incrementAndGet();
            }
            if (isRefusedByReviewer(proposal)) {
                negative.incrementAndGet();
            }
        });
        return positive.get() > 0 && negative.get() > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proposal proposal = (Proposal) o;
        return proposalID.equals(proposal.proposalID);
    }

    @Override
    public int hashCode() {
        return proposalID.hashCode();
    }

    public Set<Reviewer> getReviewers() {
        Set<Reviewer> reviewers = new HashSet<>();
        proposalReviewer.forEach(proposalReviewerElem -> {
            ProposalReviewerMapping mapping = proposalReviewerElem.getId();
            reviewers.add(mapping.getReviewer());
        });
        return reviewers;
    }
}